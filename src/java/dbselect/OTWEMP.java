/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dbselect;

import Beans.Bean;
import DB.DBConnect;
import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.jasper.tagplugins.jstl.core.Catch;

/**
 *
 * @author win8nhs20249
 */
public class OTWEMP extends HttpServlet{

    public ArrayList empover(String depno, int month, HttpServletRequest req)  throws NamingException, SQLException {

    	ArrayList empover = new ArrayList();
    	ArrayList daycounter= new ArrayList();
        //DBアクセス
       DB.DBConnect DB = new DB.DBConnect();
        try{

        	String sql = "select F_DAY,F_NAME,T_OTW.F_END from T_OTW left join T_EMPLOYEE on T_EMPLOYEE.F_EMPNO=T_OTW.F_EMPNO where T_EMPLOYEE.F_EMPNO in(select F_EMPNO from T_EMPLOYEE where F_DEPNO= ?) and F_MONTH= ?";
        	//String sql = "SELECT f_empNo,f_pass,f_name,f_DEPNO,F_POSITION FROM NICHIHAM.t_employee where f_empNo = ? and f_pass = ?";
            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            stmt.setString(1, depno);
            stmt.setInt(2, month);

            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            System.out.println(rs);
            while(rs.next()){
            	String content=rs.getString("F_NAME")+"&nbsp;&nbsp;"+rs.getString("F_END")+"まで";
            	if(Arrays.asList(daycounter).contains(rs.getString("F_DAY"))){
            		int i=daycounter.indexOf(rs.getString("F_DAY"));
            		empover.set(i, empover.get(i)+"<br />"+content);
            	}else{
            	daycounter.add(rs.getString("F_DAY"));
            	empover.add(content);
            	}

            }

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }

    	//データベースが繋がるまでテストデータ

    	/*empover.add("山田太郎&nbsp;&nbsp;21：00まで<br />山田jiro&nbsp;&nbsp;21：00まで");
    	empover.add("山田花子&nbsp;&nbsp;20：00まで");
    	empover.add("山田勘三郎&nbsp;&nbsp;23：00まで");*/

        return empover;
    }

}