/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dbselect;

import Beans.Bean;
import DB.DBConnect;
import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.jasper.tagplugins.jstl.core.Catch;

/**
 *
 * @author win8nhs20249
 */
public class OTW extends HttpServlet{

    public ArrayList selectover(String depno, int month, HttpServletRequest req)  throws NamingException, SQLException {

    	ArrayList dateover = new ArrayList();
        //DBアクセス
       DB.DBConnect DB = new DB.DBConnect();
        try{

        	String sql = "select F_DAY from T_OTW where F_EMPNO in(select F_EMPNO from T_EMPLOYEE where F_DEPNO= ? ) and F_MONTH= ?";
        	//String sql = "SELECT f_empNo,f_pass,f_name,f_DEPNO,F_POSITION FROM NICHIHAM.t_employee where f_empNo = ? and f_pass = ?";
            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            stmt.setString(1, depno);
            stmt.setInt(2, month);

            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            System.out.println(rs);
            while(rs.next()){
            	dateover.add(rs.getString("F_DAY"));
            }

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }

    	//データベースが繋がるまでテストデータ

    	/*dateover.add("15");
    	dateover.add("25");
    	dateover.add("5");*/

        return dateover;
    }

}