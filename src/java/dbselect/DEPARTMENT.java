/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dbselect;

import Beans.Bean;
import DB.DBConnect;
import java.io.IOException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.jasper.tagplugins.jstl.core.Catch;

/**
 *
 * @author win8nhs20249
 */
public class DEPARTMENT extends HttpServlet{

    public  String[] department(String empNo, HttpServletRequest req)  throws NamingException, SQLException {

    	String[] result = new String[3];
        //DBアクセス
        DB.DBConnect DB = new DB.DBConnect();
        try{

        	String sql = "select * from t_department where f_depno=(select f_depno from t_employee where f_empno=?)";
        	//String sql = "SELECT f_empNo,f_pass,f_name,f_DEPNO,F_POSITION FROM NICHIHAM.t_employee where f_empNo = ? and f_pass = ?";
            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            stmt.setString(1, empNo);
            //stmt.setString(2, pass);

            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            System.out.println(rs);
            while(rs.next()){
            	result[0]=rs.getString("F_DEPNO");
            	result[1]=rs.getString("F_DEPNAME");
            	result[2]=rs.getString("F_GROUPNAME");
            }

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }

    	//データベースが繋がるまでテストデータ

    	/*result[0]="2775";
    	result[1]="ITサービス事業部1";
    	result[2]="加工開発グループ1";*/

        return result;
    }

}