/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App;

import Beans.employeeBean;
import DB.DBConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.naming.NamingException;

/**
 *
 * @author win8nhs20249
 */
public class employee {
        private static Connection con;
        private ArrayList<HashMap<String,String>> _emps;
        private ArrayList<String> _comp;
        private ArrayList<String> _dep;
        private static DB.DBConnect DB = new DB.DBConnect();
    public ArrayList<HashMap<String,String>> getempList() throws NamingException, SQLException {
        //DBアクセス
        
        try{               
            String sql = "Select " +
                        "	emp.f_empNo," +
                        "	emp.f_name," +
                        "    dep.f_DepName," +
                        "    emp.f_class," +
                        "    emp.f_mail," +
                        "    ifnull(emp.f_image,'image') as image, " +
                        "    case" +
                        "    when " +
                        "		otw.f_empNo = ( select " +
                        "		f_empNo" +
                        "		from" +
                        "		t_otw" +
                        "		where" +
                        "		f_year = DATE_FORMAT(CurDate(),'%Y') " +
                        "               AND" +
                        "               f_month = DATE_FORMAT(CurDate(),'%m')" +
                        "                AND " +
                        "               f_day= DATE_FORMAT(CurDate(),'%d')"+
                        "		) " +
                        "    then 'あり'" +
                        "    else 'なし'"  +
                        " end  as otw " +
                        " From " +
                        "	t_employee as emp" +
                        "  left outer   Join t_department as dep On emp.f_DepNo = dep.f_DepNo" +
                        "  left outer   Join t_otw as otw On emp.f_empNo = otw.f_empNo" +
                        "    group by emp.f_empNo" +
                        ";";
            

            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            while (rs.next()) {
                HashMap<String,String> map = new HashMap<String,String>();
                String  f_empNo = rs.getString("emp.f_empNo"),
                        f_name = rs.getString("emp.f_name"),
                        f_class = rs.getString("emp.f_class"),
                        f_DepName = rs.getString("dep.f_DepName"),
                        f_mail = rs.getString("emp.f_mail"),
                        f_image = rs.getString("image"),
                        f_otw  = rs.getString("otw");
                map.put("f_empNo", f_empNo );
                map.put("f_name", f_name);
                map.put("f_class", f_class);
                map.put("f_DepName", f_DepName);
                map.put("f_mail", f_mail);
                map.put("f_image", f_image);
                map.put("f_otw",f_otw);
                emps.add(map);
                
            }
            // 取得したアイテム一覧をキャッシュしておく
            _emps = emps;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _emps;
    }
    
     public ArrayList<String> getComList() throws NamingException, SQLException {
         ArrayList<String> comp = new ArrayList<String>();
                 try{               
            String sql ="Select DISTINCT f_com From t_employee where f_com IS NOT NULL";
            

            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            int i=0;
            while (rs.next()) {
                comp.add(rs.getString("f_com")) ;
                i++;
            }
            // 取得したアイテム一覧をキャッシュしておく
            _comp = comp;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _comp;
     }
     
          public ArrayList<String> getDepList() throws NamingException, SQLException {
         ArrayList<String> dep = new ArrayList<String>();
                 try{               
            String sql ="Select DISTINCT F_DEPNAME From t_DEPARTMENT";
            

            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            int i=0;
            while (rs.next()) {
                dep.add(rs.getString("f_DEPNAME")) ;
                i++;
            }
            // 取得したアイテム一覧をキャッシュしておく
            _dep = dep;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _dep;
     }
          
         public ArrayList<HashMap<String,String>> getEmpData(String empID) throws NamingException,SQLException {
               
        try{               
            String sql =" SELECT  " +
                        "    emp.f_empNo, " +
                        "    emp.f_name, " +
                        "    dep.f_DepName, " +
                        "    emp.f_class, " +
                        "    emp.f_mail, " +
                        "    IFNULL(emp.f_image, 'image') AS image, " +
                        "    CASE " +
                        "    WHEN " +
                        "   otw.f_empNo = (SELECT  " +
                        "   f_empNo " +
                        "   FROM " +
                        "   t_otw " +
                        "   WHERE " +
                        "   f_year = DATE_FORMAT(CURDATE(), '%Y') " +
                        "   AND f_month = DATE_FORMAT(CURDATE(), '%m') " +
                        "   AND f_day = DATE_FORMAT(CURDATE(), '%d')) " +
                        "   THEN " +
                        "   'あり' " +
                        "   ELSE 'なし' " +
                        "   END AS otw " +
                        "FROM " +
                        "    t_employee AS emp " +
                        "        JOIN " +
                        "    t_department AS dep ON emp.f_DepNo = dep.f_DepNo " +
                        "        JOIN " +
                        "    t_otw AS otw ON emp.f_empNo = otw.f_empNo " +
                        "WHERE " +
                        "	emp.f_empNo = '"+ empID+"' " +
                        "GROUP BY emp.f_empNo";
            
            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            while (rs.next()) {
                HashMap<String,String> map = new HashMap<String,String>();
                String  f_empNo = rs.getString("emp.f_empNo"),
                        f_name = rs.getString("emp.f_name"),
                        f_class = rs.getString("emp.f_class"),
                        f_DepName = rs.getString("dep.f_DepName"),
                        f_mail = rs.getString("emp.f_mail"),
                        f_image = rs.getString("image"),
                        f_otw  = rs.getString("otw");
                map.put("f_empNo", f_empNo );
                map.put("f_name", f_name);
                map.put("f_class", f_class);
                map.put("f_DepName", f_DepName);
                map.put("f_mail", f_mail);
                map.put("f_image", f_image);
                map.put("f_otw",f_otw);
                emps.add(map);
                
            }
            // 取得したアイテム一覧をキャッシュしておく
            _emps = emps;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _emps; 
    }
}
