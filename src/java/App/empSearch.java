/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package App;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.naming.NamingException;

/**
 *
 * @author win8nhs20249
 */
public class empSearch {
    private static Connection con;
        private ArrayList<HashMap<String,String>> _emps;
        private ArrayList<String> _comp;
        private ArrayList<String> _dep;
        private static DB.DBConnect DB = new DB.DBConnect();
    public ArrayList<HashMap<String,String>> getempList(String sql ,String empName ,String depart ,String classname ,String com) throws NamingException, SQLException {
        //DBアクセス
        
        try{               
            
            
            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            while (rs.next()) {
                HashMap<String,String> map = new HashMap<String,String>();
                String  f_empNo = rs.getString("emp.f_empNo"),
                        f_name = rs.getString("emp.f_name"),
                        f_class = rs.getString("emp.f_class"),
                        f_DepName = rs.getString("dep.f_DepName"),
                        f_mail = rs.getString("emp.f_mail"),
                        f_image = rs.getString("image"),
                        f_otw  = rs.getString("otw");
                map.put("f_empNo", f_empNo );
                map.put("f_name", f_name);
                map.put("f_class", f_class);
                map.put("f_DepName", f_DepName);
                map.put("f_mail", f_mail);
                map.put("f_image", f_image);
                map.put("f_otw",f_otw);
                emps.add(map);
                
            }
            // 取得したアイテム一覧をキャッシュしておく
            _emps = emps;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _emps;
    }
    
     public ArrayList<String> getComList() throws NamingException, SQLException {
         ArrayList<String> comp = new ArrayList<String>();
                 try{               
            String sql ="Select DISTINCT f_com From t_employee where f_com IS NOT NULL";
            

            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            int i=0;
            while (rs.next()) {
                comp.add(rs.getString("f_com")) ;
                i++;
            }
            // 取得したアイテム一覧をキャッシュしておく
            _comp = comp;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _comp;
     }
     
          public ArrayList<String> getDepList() throws NamingException, SQLException {
         ArrayList<String> dep = new ArrayList<String>();
                 try{               
            String sql ="Select DISTINCT F_DEPNAME From t_DEPARTMENT";
            

            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            ArrayList<HashMap<String,String>> emps = new ArrayList<HashMap<String,String>>();
            
            int i=0;
            while (rs.next()) {
                dep.add(rs.getString("f_DEPNAME")) ;
                i++;
            }
            // 取得したアイテム一覧をキャッシュしておく
            _dep = dep;

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return _dep;
     }
    
}
