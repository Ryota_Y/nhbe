/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import java.io.IOException;

//HttpServlet系のライブラリインポート
import javax.servlet.ServletException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



//SQLライブラリのインポート
import java.sql.*;

/**
 *
 * @author nhs20018
 */
public class otimein
     extends HttpServlet{
	public void doPost
	( HttpServletRequest req,HttpServletResponse res)
	throws IOException, ServletException
	{
		Edit(req, res);
	}

	public void doGet
	( HttpServletRequest req, HttpServletResponse res)
	throws IOException, ServletException
	{
		Edit(req, res);
	}

	//doGetとdoPostの中身を
	//関数化する
	public void Edit(HttpServletRequest req,HttpServletResponse res)
	throws ServletException, IOException
	{
             HttpSession session = req.getSession(true);
            //文字コード設定
            res.setCharacterEncoding("UTF-8");
            req.setCharacterEncoding("UTF-8");
            
            //値の受け取り
            String year=null;
            year=(String)req.getParameter("year");

            String month=null;
            month=(String)req.getParameter("month");
            
            String day=null;
            day=(String)req.getParameter("day");
            
            String time=null;
            time=(String)req.getParameter("time");
  
            String minite=null;
            minite=(String)req.getParameter("minite");

            String time_e=null;
            time_e=(String)req.getParameter("time_e");


            String minite_e=null;
            minite_e=(String)req.getParameter("minite_e");
            
            String empno=null;
            //テストのために仮の値
            //empno="31_001";
            //本来はこっち
            empno=(String)session.getAttribute("f_empNo");

            String USER = "root";
            String PASSWORD = "root";
            String URL = "jdbc:mysql://localhost/nichiham";

            Connection con = null;
            Statement stmt = null;
            StringBuffer SQL = null;
            ResultSet rs = null;
            String sql=null;

            String DRIVER= "com.mysql.jdbc.Driver";




            String MSG = "NG";


            //例外処理用メッセージ格納
            StringBuffer ERMSG = null;

            try{
//              JDBCドライバのロード
                Class.forName(DRIVER).newInstance();

//		Connectionオブジェクトの作成
                con = DriverManager.getConnection(URL,USER,PASSWORD);

		//Statementオブジェクトの作成
		stmt = con.createStatement();

		/////////////

		//SQLステートメントの作成（選択クエリー）
		SQL = new StringBuffer();
		sql="INSERT INTO t_otw(F_EMPNO,F_START,F_END,F_ACCEPT,F_YEAR,F_MONTH,F_DAY)"
                        + " VALUES('" +empno + "','"+time+":"+minite+ ":00','"+time_e+":"+minite_e+":00','"
                         +0+"','"+year+"','"+month+"','"+day+"');";
		stmt.executeUpdate(sql);

            //tryブロックの終了
            //例外処理 catchブロック
            }catch(ClassNotFoundException e){
		ERMSG = new StringBuffer();
		ERMSG.append(e.getMessage());
                System.out.println(ERMSG);
            }
            catch(SQLException e){
		ERMSG = new StringBuffer();
		ERMSG.append(e.getMessage());
                System.out.println(ERMSG);
            }
            catch(Exception e){
		ERMSG = new StringBuffer();
		ERMSG.append(e.getMessage());
                System.out.println(ERMSG);
            }
            finally{
            //各種オブジェクトのクローズ
            try{
                if(rs != null){
                    rs.close();
                }
                if(stmt != null){
                    stmt.close();
                }

                if(con != null){
                    con.close();
                }
            }
            catch(SQLException e){
		ERMSG = new StringBuffer();
		ERMSG.append(e.getMessage());
                System.out.println(ERMSG);
            }
	}
            //サーブレットからJSPへ渡す
            System.out.println(sql);
            req.setAttribute("sql",sql);
	    ServletContext sc = null;
	    sc = getServletContext();
	    sc.getRequestDispatcher("/otimein.jsp").forward(req,res);

	}

}