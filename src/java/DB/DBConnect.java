/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DB;

import Beans.loginBean;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author win8nhs20249
 */
public class DBConnect {
    private static Connection con;
    public synchronized Connection DBaccess () throws NamingException,SQLException{
        InitialContext context = new InitialContext();
        DataSource ds = (DataSource) context.lookup("java:/comp/env/jdbc_mysql");
        con = ds.getConnection();
        System.out.println("access DONE!!");
        return con;
    }
    
  //  public Connection GetConnection() throws NamingException,SQLException{
  //     this.con = con;
  //      System.out.println("GetConnection");
  //      return con;
  //  }
    
    public void close() throws SQLException{
        try{
            con.close();
        }catch(SQLException se){
            con = null;
            System.out.println();
            throw se;
        }
    }
}
