/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Beans;

/**
 *
 * @author win8nhs20284
 */

public class InfoBeans {

	//プロパティの宣言
		//要員情報 T_EMPLOYEE
		private String EmpNo;		//要員NO 			F_EMPNO		← 貸出情報 & 残業申請 (& 座席表)
		private String EmpName;		//名前 				F_NAME
		private String EmpKana;		//ナマエ 			F_NAMEKANA
		private String EmpPlace;	//勤務地			F_PLACE
		private String EmpAd;		//AD				F_AD
		private String EmpStat;		//在籍状況			F_STATUS
		private String EmpClass;	//社員区分			F_CLASS
		private String EmpId;		//社員番号			F_EID
		private String EmpPosi;		//役職				F_POSITION
		private String EmpDno;		//事業所CD			F_DENPNO	→ 所属
		private String EmpCno;		//カードNO			F_CARDNO	→ カード情報)
		private String EmpCom;		//協力会社名		F_COM
		private String EmpStart;	//在籍開始日		F_START
		private String EmpEnd;		//在籍終了日		F_END
		private String EmpImg;		//画像名			F_IMAGE
		private String EmpRingi;	//稟議NO			F_RINGI
		private String EmpCont;		//契約締結日		F_COM
		private String EmpPass;		//パスワード		F_PASS
		private String EmpUp;		//最終更新日		F_UPDATE

		//所属 T_DEPATMENT
		private String EmpDname;	//事業所名			F_DEMPNAME
		private String EmpDgroup;	//グループ名		F_GROUPNAME

		//カード情報 T_CARD
		private String EmpCdist;	//配布日			F_DISTRIBUTE
		private String EmpColl;		//回収日			F_COLLECT

		//貸出情報 T_LEASE
		private String EmpItno;		//IT機器NO			F_INTO
		private String EmpIlease;	//リース番号		F_LEASE
		private String EmpIstart;	//貸出開始日		F_START
		private String EmpIend;		//貸出終了日		F_END
		private String EmpIserial;	//シリアルコード	F_SERIAL

		//残業申請 T_OTW
		private String EmpTdate;	//残業日			F_DATE
		private String EmpTstart;	//残業開始時刻		F_START
		private String EmpTend;		//残業終了時刻		F_END
		private String EmpTaccept;	//承認				F_ACCEPT

		//残業禁止日 T_NOTOTW
		private String EmpTmonth;	//月				F_MONTH
		private String EmpTday;		//日				F_DAY

		/*
		//座席表 T_SEAT
		private String EmpSeat;		//座席				F_SEATMAP
		private String EmpStel;		//内線				F_TEL
		private String EmpSout;		//外線				F_OUTTEL
		*/

		//コンストラクタ
		public InfoBeans(){
			//TODO 自動生成されたコンストラクタ
			//プロパティの初期化

			//要員情報
			this.EmpNo = "";
			this.EmpName = "";
			this.EmpKana = "";
			this.EmpPlace = "";
			this.EmpAd = "";
			this.EmpStat = "";
			this.EmpClass = "";
			this.EmpId = "";
			this.EmpPosi = "";
			this.EmpDno = "";
			this.EmpCno = "";
			this.EmpCom = "";
			this.EmpStart = "";
			this.EmpEnd = "";
			this.EmpImg = "";
			this.EmpRingi = "";
			this.EmpCont = "";
			this.EmpPass = "";
			this.EmpUp = "";
			//所属
			this.EmpDname = "";
			this.EmpDgroup = "";
			//カード情報
			this.EmpCdist = "";
			this.EmpColl = "";
			//貸出情報
			this.EmpItno = "";
			this.EmpIlease = "";
			this.EmpIstart = "";
			this.EmpIend = "";
			this.EmpIserial = "";
			//残業申請
			this.EmpTdate = "";
			this.EmpTstart = "";
			this.EmpTend = "";
			this.EmpTaccept = "";
			//残業禁止日
			this.EmpTmonth= "";
			this.EmpTday = "";
			/*
			//座席表
			this.EmpSeat= "";
			this.EmpStel= "";
			this.EmpSout= "";
			*/
		}


		//セッターの宣言
		//要員情報
		public void setEmpNo(String in_EmpNo){
			this.EmpNo = in_EmpNo;
		}
		public void setEmpName(String in_EmpName){
			this.EmpName = in_EmpName;
		}
		public void setEmpKana(String in_EmpKana){
			this.EmpKana = in_EmpKana;
		}
		public void setEmpPlace(String in_EmpPlace){
			this.EmpPlace = in_EmpPlace;
		}
		public void setEmpAd(String in_EmpAd){
			this.EmpAd = in_EmpAd;
		}
		public void setEmpStat(String in_EmpStat){
			this.EmpStat = in_EmpStat;
		}
		public void setEmpClass(String in_EmpClass){
			this.EmpClass = in_EmpClass;
		}
		public void setEmpId(String in_EmpId){
			this.EmpId = in_EmpId;
		}
		public void setEmpPosi(String in_EmpPosi){
			this.EmpPosi = in_EmpPosi;
		}
		public void setEmpDno(String in_EmpDno){
			this.EmpDno = in_EmpDno;
		}
		public void setEmpCno(String in_EmpCno){
			this.EmpCno = in_EmpCno;
		}
		public void setEmpCom(String in_EmpCom){
			this.EmpCom = in_EmpCom;
		}
		public void setEmpStart(String in_EmpStart){
			this.EmpStart = in_EmpStart;
		}
		public void setEmpEnd(String in_EmpEnd){
			this.EmpEnd = in_EmpEnd;
		}
		public void setEmpImg(String in_EmpImg){
			this.EmpImg = in_EmpImg;
		}
		public void setEmpRingi(String in_EmpRingi){
			this.EmpRingi = in_EmpRingi;
		}
		public void setEmpCont(String in_EmpCont){
			this.EmpCont = in_EmpCont;
		}
		public void setEmpPass(String in_EmpPass){
			this.EmpPass = in_EmpPass;
		}
		public void setEmpUp(String in_EmpUp){
			this.EmpUp = in_EmpUp;
		}
		//所属
		public void setEmpDname(String in_EmpDname){
			this.EmpDname = in_EmpDname;
		}
		public void setEmpDgroup(String in_EmpDgroup){
			this.EmpDgroup = in_EmpDgroup;
		}
		//カード情報
		public void setEmpCdist(String in_EmpCdist){
			this.EmpCdist = in_EmpCdist;
		}
		public void setEmpColl(String in_EmpColl){
			this.EmpColl = in_EmpColl;
		}
		//貸出情報
		public void setEmpItno(String in_EmpItno){
			this.EmpItno = in_EmpItno;
		}
		public void setEmpIlease(String in_EmpIlease){
			this.EmpIlease = in_EmpIlease;
		}
		public void setEmpIstart(String in_EmpIstart){
			this.EmpIstart = in_EmpIstart;
		}
		public void setEmpIend(String in_EmpIend){
			this.EmpIend = in_EmpIend;
		}
		public void setEmpIserial(String in_EmpIserial){
			this.EmpIserial = in_EmpIserial;
		}
		//残業申請
		public void setEmpTdate(String in_EmpTdate){
			this.EmpTdate = in_EmpTdate;
		}
		public void setEmpTstart(String in_EmpTstart){
			this.EmpTstart = in_EmpTstart;
		}
		public void setEmpTend(String in_EmpTend){
			this.EmpTend = in_EmpTend;
		}
		public void setEmpTaccept(String in_EmpTaccept){
			this.EmpTaccept = in_EmpTaccept;
		}
		//残業禁止日
		public void setEmpTmonth(String in_EmpTmonth){
			this.EmpTmonth = in_EmpTmonth;
		}
		public void setEmpTday(String in_EmpTday){
			this.EmpTday = in_EmpTday;
		}
		/*
		//座席表
		public void setEmpSeat(String in_EmpSeat){
			this.EmpSeat = in_EmpSeat;
		}
		public void setEmpStel(String in_EmpStel){
			this.EmpStel = in_EmpStel;
		}
		public void setEmpSout(String in_EmpSout){
			this.EmpSout = in_EmpSout;
		}
		*/

		//ゲッターの宣言
		//要員情報
		public String  getEmpNo(){
			return this.EmpNo;
		}
		public String  getEmpName(){
			return this.EmpName;
		}
		public String  getEmpKana(){
			return this.EmpKana;
		}
		public String  getEmpPlace(){
			return this.EmpPlace;
		}
		public String  getEmpAd(){
			return this.EmpAd;
		}
		public String  getEmpStat(){
			return this.EmpStat;
		}
		public String  getEmpClass(){
			return this.EmpClass;
		}
		public String  getEmpId(){
			return this.EmpId;
		}
		public String  getEmpPosi(){
			return this.EmpPosi;
		}
		public String  getEmpDno(){
			return this.EmpDno;
		}
		public String  getEmpCno(){
			return this.EmpCno;
		}
		public String  getEmpCom(){
			return this.EmpCom;
		}
		public String  getEmpStart(){
			return this.EmpStart;
		}
		public String  getEmpEnd(){
			return this.EmpEnd;
		}
		public String  getEmpImg(){
			return this.EmpImg;
		}
		public String  getEmpRingi(){
			return this.EmpRingi;
		}
		public String  getEmpCont(){
			return this.EmpCont;
		}
		public String  getEmpPass(){
			return this.EmpPass;
		}
		public String  getEmpUp(){
			return this.EmpUp;
		}
		//所属
		public String  getEmpDname(){
			return this.EmpDname;
		}
		public String  getEmpDgroup(){
			return this.EmpDgroup;
		}
		//カード情報
		public String  getEmpCdist(){
			return this.EmpCdist;
		}
		public String  getEmpColl(){
			return this.EmpColl;
		}
		//貸出情報
		public String  getEmpItno(){
			return this.EmpItno;
		}
		public String  getEmpIlease(){
			return this.EmpIlease;
		}
		public String  getEmpIstart(){
			return this.EmpIstart;
		}
		public String  getEmpIend(){
			return this.EmpIend;
		}
		public String  getEmpIserial(){
			return this.EmpIserial;
		}
		//残業申請
		public String  getEmpTdate(){
			return this.EmpTdate;
		}
		public String  getEmpTstart(){
			return this.EmpTstart;
		}
		public String  getEmpTend(){
			return this.EmpTend;
		}
		public String  getEmpTaccept(){
			return this.EmpTaccept;
		}
		//残業禁止日
		public String  getEmpTmonth(){
			return this.EmpTmonth;
		}
		public String  getEmpTday(){
			return this.EmpTday;
		}
		/*
		//座席表
		public String  getEmpSeat(){
			return this.EmpSeat;
		}
		public String  getEmpStel(){
			return this.EmpStel;
		}
		public String  getEmpSout(){
			return this.EmpSout;
		}
		*/



}

