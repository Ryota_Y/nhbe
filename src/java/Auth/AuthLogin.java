/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Auth;

import Beans.loginBean;
import DB.DBConnect;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.jasper.tagplugins.jstl.core.Catch;

/**
 *
 * @author win8nhs20249
 */
public class AuthLogin extends HttpServlet{


    private static Connection con;
    
    public boolean login(String empNo, String pass ,HttpServletRequest req)  throws NamingException, SQLException {
        boolean result = false;
        HttpSession session = req.getSession(true);
        //DBアクセス
        DB.DBConnect DB = new DB.DBConnect();
        try{   
            
            String sql = "SELECT f_empNo,f_pass,f_name,f_DEPNO,F_POSITION FROM NICHIHAM.t_employee where f_empNo = ? and f_pass = ?";
            PreparedStatement stmt = DB.DBaccess().prepareStatement(sql);
            stmt.setString(1, empNo);
            stmt.setString(2, pass);
            
            System.out.println("GetConnect DONE!!");
            ResultSet rs = stmt.executeQuery();
            System.out.println("exequteQuery DONE!!");
            System.out.println(rs);
            while(rs.next()){
                if(result == false){
                    result = true;   
                }
                session.setAttribute("f_empNo", rs.getString("f_empNo"));
                session.setAttribute("f_DEPNO", rs.getString("f_DEPNO"));
                session.setAttribute("f_POSITION", rs.getString("F_POSITION"));
                session.setAttribute("f_name", rs.getString("f_name"));
                session.setAttribute("f_pass", rs.getString("f_pass"));
            }

        }catch(SQLException se){
            System.out.println("SQLexception="+se);
        }finally{
            DB.close();
            System.out.println("close DONE!!");
        }
        return result;
    }

}