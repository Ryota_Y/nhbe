<%--
    Document   : login
    Created on : 2015/06/17, 17:58:26
    Author     : win8nhs20284
--%>

<%@page import="dbselect.DEPARTMENT"%>
<%@page import="Beans.*,dbselect.*" %>
<%@page import="java.util.Calendar" %>
<%@page import="java.util.*" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<title>残業一覧ページ</title>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		<script src="js/jquery.darktooltip.js"></script>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/long.js"></script>
		<link rel="stylesheet" href="css/darktooltip.css">
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/sugiyama.css">
		<link rel="stylesheet" type="text/css" href="css/cheng.css">
	</head>
<%

	//セッションから社員番号を取る
	String empNo=(String)session.getAttribute("f_empNo");
        String name = session.getAttribute("f_name").toString();
	//System.out.println(empNo);

	//部署名を取り出すための処理
	dbselect.DEPARTMENT depar = new DEPARTMENT();
	String[] depare=depar.department(empNo, request);

    //System.out.println(depare[2]);

    //月を取得するための処理
    Calendar calendar = Calendar.getInstance();
    int mon=calendar.get(Calendar.MONTH) + 1;
    int yea=calendar.get(Calendar.YEAR);
    //System.out.println(mon);
    //System.out.println(yea);

    String monbefore="";
    String monafter="";

    monbefore = request.getParameter("before");
    monafter = request.getParameter("after");

    if(monafter!=null&&monbefore==null){
    	int monthnow = Integer.valueOf(request.getParameter("monthnow"));
    	mon=monthnow+1;
    	if(mon>12){
        	mon=1;
        	yea++;
        }
    }else if(monbefore!=null&&monafter==null){
    	int monthnow = Integer.valueOf(request.getParameter("monthnow"));
    	mon=monthnow-1;
    	 if(mon<1){
    	    	mon=12;
    	    	yea--;
    	    }
    }

  	//残業不可日を取り出すための処理
  	dbselect.NOTOTW dateno = new NOTOTW();
  	ArrayList redateno=dateno.selectnoto(depare[0],mon,request);

  	//残業日を抽出するための処理
  	dbselect.OTW dateover = new OTW();
  	ArrayList redateover=dateover.selectover(depare[0],mon,request);

  	//残業する人の情報を取り出す処理
  	dbselect.OTWEMP empover = new OTWEMP();
  	ArrayList rempover=empover.empover(depare[0],mon,request);

%>

	<body>

		<h1>
		<a href="index.jsp"><!--ロゴ-->
			<img src="images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->

		<div id="login">
			<span id="out"><%=name%>　様</span>
			<a href="login.jsp"><img src="images/logout.gif" id="log_img"></a>
		</div>

	<!--検索-->
	<div id="search">

		<div id="easy_but">
			<form action="daytime.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/over_work.gif" value="今日残業する人">
			</form>
			<form action="lastout.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/last_exit.gif" value="最後退室">
			</form>
			<form action="mypage.jsp" method="post" class="slong">
				<input type="image" src="images/mypage.gif" value="マイページ">
			</form>
		</div>
		<form action="#" method="post" id="search">
			<span id="button">
				検索条件：
				部署:
                                <select name="place">
					<option value="0">-----</option>
					<option value="1">管理部</option>
					<option value="2">営業部</option>
					<option value="3">開発部</option>
				</select>




				<button type="submit" class="img_but">
					<img src="images/search_19.gif" width="60" height="18">
				</button>

				<button type="reset" class="img_but">
  					<img src="images/reset_19.gif">
  				</button>
  			</span>
		</form>


<br />

	<div id="view">
		<span class="text_top"><a href="otime.jsp" >残業一覧</a></span>
	</div>

	<div id="sighin">
		<span class="text_top"><a href="otimein.jsp">残業登録</a></span>
	</div>

	<div id="notsigh">
		<span class="text_top"><a href="nototime.jsp">残業不可登録</a></span>
	</div>


	</div>
	<!--検索終了-->

	<!--タグの始まり-->
		<div id="container">
		<!--具体内容-->

			<ul class="panel">

				<!--残業登録画面カレンダー形式-->
				<li id="tab3">
				<div class="category"><span class="caltitle"><%= depare[2]%>　残業一覧</span><br />
					<script type="text/javascript">
						myDate    = new Date();                                    // 今日の日付データ取得
						myWeekTbl = new Array("日","月","火","水","木","金","土");  // 曜日テーブル定義
						myMonthTbl= new Array(31,28,31,30,31,30,31,31,30,31,30,31);// 月テーブル定義

						myMonth = <%=mon%>-1;                                      //該当月を取得
						myDate.setMonth(myMonth);                                  //月を設定する
						myYear = <%=yea%>;                                         //年を取得
						myDate.setFullYear(myYear);                                //年を設定する

						if (((myYear%4)==0 && (myYear%100)!=0) || (myYear%400)==0){ // うるう年だったら...
						   myMonthTbl[1] = 29;                                     // 　２月を２９日とする
						}

						myToday = myDate.getDate();                                // 今日の'日'を退避
						myDate.setDate(1);                                         // 日付を'１日'に変えて、
						myWeek = myDate.getDay();                                  // 　'１日'の曜日を取得
						myTblLine = Math.ceil((myWeek+myMonthTbl[myMonth])/7);     // カレンダーの行数
						myTable   = new Array(7*myTblLine);                        // 表のセル数分定義

						var year_now=myYear;
						var month_now=myMonth+1;

						//var nototime=[1,9,17,23];                                          //残業不可日の配列
						var nototime=[];
						<%for(int i=0;i<redateno.size();i++){%>
							nototime[<%=i%>]=<%=redateno.get(i)%>;
						<%}%>

						//var otime=[2,12,18,24];                                             //残業日の配列
						var otime=[];
						<%for(int i=0;i<redateover.size();i++){%>
						otime[<%=i%>]=<%=redateover.get(i)%>;
						<%}%>

						//var omember=["山田太郎&nbsp;&nbsp;21：00まで<br />山田jiro&nbsp;&nbsp;21：00まで","山田花子&nbsp;&nbsp;20：00まで","山田勘三郎&nbsp;&nbsp;23：00まで"];                //残業する人間の配列
						var omember=[];
						<%for(int i=0;i<rempover.size();i++){%>
						omember[<%=i%>]="<%=rempover.get(i)%>";
						<%}%>

						document.write('<form action="" submit="post" class="calform">');
						document.write('<input type="submit" id="last_month" value="<<" name="before">');
						document.write('<span id="year_month">',year_now,'年',month_now,'月</span>');
						document.write('<input type="submit" id="next_month" value=">>" name="after">');
						document.write('<input type="hidden" id="next_month" value="'+<%=mon%>+'" name="monthnow">');
						document.write('</form>');

						for(i=0; i<7*myTblLine; i++) myTable[i]="　";              // myTableを掃除する
							for(i=0; i<myMonthTbl[myMonth]; i++)myTable[i+myWeek]=i+1; // 日付を埋め込む


								document.write("<table id='calendar'>");      // 表の作成開始
								document.write("<tr>");
								for(i=0; i<7; i++){                                        // 一行(１週間)ループ
   									document.write("<th align='center' ");
   										if(i==0)document.write("bgcolor='#fa8072'>");           // 日曜のセルの色
   										else    document.write("bgcolor='#ffebcd'>");           // 月～土のセルの色
   									document.write("<strong>",myWeekTbl[i],"</strong>");    // '日'から'土'の表示
   									document.write("</th>");
								}
								document.write("</tr>");
									var cnt_member=0;//残業する人を表示するためのカウンタ
								for(i=0; i<myTblLine; i++){                                // 表の「行」のループ
   									document.write("<tr>");                                 // 行の開始
   								for(j=0; j<7; j++){                                     // 表の「列」のループ
      								document.write("<td align='center' ");               // 列(セル)の作成
      								myDat = myTable[j+(i*7)];                            // 書きこむ内容の取得
      								if (otime.indexOf(myDat)>=0)document.write(" class='red'>"); // 残業あがある日
      								else if(j==0||j==6||nototime.indexOf(myDat)>=0)      document.write(" data-status='not' class='gray'>"); // 残業不可日
      								else               document.write("bgcolor='#ffffe0'>"); // 平日のセルの色

      								if (otime.indexOf(myDat)>=0){document.write("<strong><a href='#' class='example' data-tooltip='",omember[cnt_member],"'>",myDat,"</a></strong>");cnt_member++;        // 残業する日をを表示
      								}else {document.write("<strong>",myDat,"</strong>");}        // 日付セット
      								document.write("</td>");                             // 列(セル)の終わり
							}
							document.write("</tr>");                                // 行の終わり
						}
						document.write("</table>");                                // 表の終わり

				//document.write("</div>");
				</script>

		</div><!--タグの終了-->



		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxR">
					<img src="images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>



	</body>
</html>