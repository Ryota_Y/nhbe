<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<title>パスワード変更確認ページ</title>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jsindex.js"></script>
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/sugiyama.css">
	</head>



	<body>
	
	
	<h1>
		<a href="index.html"><!--ロゴ-->
			<img src="images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->

		<div id="login">
			<span id="out">山田太郎　様</span>
			<a href="login.html"><img src="images/logout.gif" id="log_img"></a>
		</div>

	<!--検索-->
	<div id="search">
		
		<div id="easy_but">
			<form action="daytime.html" method="post" class="slong" id="easy_left">
				<input type="image" src="images/over_work.gif" value="今日残業する人">
			</form>
			<form action="lastout.html" method="post" class="slong" id="easy_left">
				<input type="image" src="images/last_exit.gif" value="最後退室">
			</form>
			<form action="mypage.html" method="post" class="slong">
				<input type="image" src="images/mypage_select.gif" value="マイページ">
			</form>
		</div>
<br />

	<div id="view">
		<span class="text_top"><a href="otime.html" >残業一覧</a></span>
	</div>
	
	<div id="sighin">
		<span class="text_top"><a href="otimein.html">残業登録</a></span>
	</div>
	
	<div id="notsigh">
		<span class="text_top"><a href="nototime.html">残業不可登録</a></span>
	</div>
	

	</div>
	<!--検索終了-->
	
				<!--具体内容-->
		<div id="container">
		<!--具体内容-->
			<ul class="panel">
			<!--登録フォーム-->
				<li id="tab1">
				<div class="category">
					<h4>パスワード変更確認</h4>
					パスワード変更をしてよろしいですか？
					<form method="post" action="endpass.html">
						<input type="hidden" name="pass" value="">
						<input type="hidden" name="newpass" value="">
						<input type="hidden" name="newpass2" value="">
					<input type="submit" value="パスワード変更">
					</form>
					<br />
					<a href="mypage.html">戻る</a>
				</div>

				</li><!--登録フォーム完了-->
			</ul><!--具体内容終了-->
		</div>

		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxR">
					<img src="images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>
	</body>