<%-- 
    Document   : mypage
    Created on : 2015/06/24, 17:38:34
    Author     : win8nhs20249
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<title>マイページ</title>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jsindex.js"></script>
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/sugiyama.css">
	</head>
<%
            session = request.getSession(false);
            String name = "none";
            String pass = "none";
            String empNo = "none";
            if(!session.isNew()){
                 name = session.getAttribute("f_name").toString();
                 pass = session.getAttribute("f_pass").toString();
                 empNo = session.getAttribute("f_empNo").toString();
            }
            
            App.employee AppEmp = new App.employee();
            ArrayList<HashMap<String,String>> emps ;
            emps = AppEmp.getEmpData(empNo);
%>
	<body>
	
	<h1>
		<a href="index.jsp"><!--ロゴ-->
			<img src="images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->

		<div id="login">
			<span id="out"><%=name %>　様</span>
			<a href="login.jsp"><img src="images/logout.gif" id="log_img"></a>
		</div>

	<!--検索-->
	<div id="search">
		
		<div id="easy_but">
			<form action="daytime.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/over_work.gif" value="今日残業する人">
			</form>
			<form action="lastout.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/last_exit.gif" value="最後退室">
			</form>
			<form action="mypage.jsp" method="post" class="slong">
				<input type="image" src="images/mypage_select.gif" value="マイページ">
			</form>
		</div>
<br />

	<div id="view">
		<span class="text_top"><a href="otime.jsp" >残業一覧</a></span>
	</div>
	
	<div id="sighin">
		<span class="text_top"><a href="otimein.jsp">残業登録</a></span>
	</div>
	
	<div id="notsigh">
		<span class="text_top"><a href="nototime.jsp">残業不可登録</a></span>
	</div>
	

	</div>
	<!--検索終了-->
	
	<!--タグの始まり-->
		<div id="container">
		<!--具体内容-->
			<ul class="panel">
			<!--要員一覧-->
				<li id="tab1">

				<!--要員具体内容-->
				<!--要員一　繰り返し項目-->
				<!--要員一-->
				<div class="category lightgray">
					<%
                                            for(int i = 0;i < emps.size();i++){
%>

					<table class="border">
                                                <tr>
                                                    <td rowspan="5"><img src="images/syain1.gif" width="175px" height="200px"alt="社員の写真"></td>
							<td>名前</td>
							<td class="tdright"><%=emps.get(i).get("f_name")%></td>
							<td>部署</td>
							<td class="tdright"><%=emps.get(i).get("f_DepName")%></td>

                                                </tr>
							
						<tr>
							<td>契約形態</td>
							<td class="tdright"><%=emps.get(i).get("f_class")%></td>
							<td>残業</td>
							<td class="tdright"><%=emps.get(i).get("f_otw")%></td>
						</tr>
						<tr>
							<td>メールアドレス</td>
							<td class="tdright" colspan="3"><a href="<%=emps.get(i).get("f_mail")%>"><%=emps.get(i).get("f_mail")%></a></td>
						</tr>
					</table><br />
<%     
                                            }             
%>
				</div>
				<!--要員一完了-->
				<br /><hr />
				<!--要員具体内容完了-->

				</li><!--要員一覧終了-->
                                <style type="text/css">
                                <!--
                                a#changePassMessage{
                                color:#FF0000;
                                      }
                                a#inputPassMessage{
                                color:#FF0000;
                                      }-->
                                </style>
				
				<li><!-- パスワード更新 -->
					<h4>パスワード変更欄</h4>
					<form method="post" action="checkpass.jsp">
                                            <input type="hidden" value="<%=pass%>" name="currPass" id="currPass">
                                                <a id="changePassMessage"　></a>
                                                <a id="inputPassMessage"　></a>
						<table>
							<tr>
								<td>現在のパスワード      ：</td>
                                                                <td><input type="password" name="pass" maxlength="15" onchange="ctrlButton()" id="pass"></td>
							</tr>
							<tr>
								<td>新規のパスワード      ：</td>
                                                                <td><input type="password" name="newpass" maxlength="15" id="newPass" onchange="ctrlButton()"></td>
							</tr>
							<tr>
								<td>新規のパスワード確認  ：</td>
                                                                <td><input type="password" name="newpass2" maxlength="15" id="newPassCheck" onchange="ctrlButton()"></td>
							</tr>
						</table>
						<input type="submit" value="パスワード変更" name="changePass" id="changePass">
					</form>
				</li><!-- パスワード更新終了 -->

			</ul><!--具体内容終了-->
		</div><!--タグの終了-->

		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxR">
					<img src="images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>
	</body>
                                                <%
                                                System.out.println(pass);
                                                %>
</html>
                                
<script>

    function ctrlButton(){
        var currPass  = document.getElementById("currPass").value;
        var Pass = document.getElementById("pass").value;
        var newPass = document.getElementById("newPass").value;
        var newPassCheck = document.getElementById("newPassCheck").value;
        console.log(currPass);
        console.log(Pass);
        console.log(newPass);
        console.log(newPassCheck);
        if(currPass == Pass){
                document.getElementById("changePassMessage").textContent ="";
                document.getElementById("changePass").disabled = false;
                document.getElementById("changePass").enabled = true;
                console.log("有効");
        }else{
            document.getElementById("changePassMessage").textContent = '現在のパスワードが登録されたものと異なります。';
            document.getElementById("changePass").disabled = true;
            console.log("無効化");
        }
        
        if( newPass && newPassCheck && newPass == newPassCheck){
                document.getElementById("inputPassMessage").textContent = '';
                document.getElementById("changePass").disabled = false;
                document.getElementById("changePass").enabled = true;
                console.log("確認Pass");
  
        }else{
                document.getElementById("inputPassMessage").textContent = '新規のパスワードと確認用パスワードの値が異なります。';
                document.getElementById("changePass").disabled = true;
                console.log("確認Block");
        }
    }
    
    function blockChangeButton(){
        document.getElementById("changePass").disabled = true;
    }
    
    window.onload= blockChangeButton();


</script>