<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<title>社員ページ</title>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jsindex.js"></script>
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/sugiyama.css">
	</head>



	<body>
	<%
 
            session = request.getSession(false);
            String name = "none";
            if(!session.isNew()){
                 name = session.getAttribute("f_name").toString();
            }
         
            App.employee AppEmp = new App.employee();
            ArrayList<HashMap<String,String>> emps ;
            ArrayList<String> comp  ;
            ArrayList<String> dep ;
            String sql ;
            request.setCharacterEncoding("UTF-8");
           if(request.getMethod().equals("POST") && request.getParameter("name")!=null || request.getParameter("place")!=null || request.getParameter("class")!=null || request.getParameter("com")!=null)
           {
                //検索時
               App.empSearch AppempS = new App.empSearch();
               
               String empName    = request.getParameter("name").toString(),
                      depart     = request.getParameter("place"),
                      className  = request.getParameter("class"),
                      com        = request.getParameter("com");
               if(empName==null){
                   empName= "";
               }
               if(depart==null){
                   depart= "";
               }
               if(className==null){
                   className= "";
               }
               if(com==null){
                   com= "";
               }
                System.out.println(empName);
                System.out.println(depart);
                System.out.println(className);
                System.out.println(com);
                 sql =  "Select " +
                        "	emp.f_empNo," +
                        "	emp.f_name," +
                        "    dep.f_DepName," +
                        "    emp.f_class," +
                        "    emp.f_mail," +
                        "    ifnull(emp.f_image,'image') as image, " +
                        "    case" +
                        "    when " +
                        "       otw.f_empNo = ( select " +
                        "	f_empNo" +
                        "	from" +
                        "	t_otw" +
                        "	where" +
                        "	f_year = DATE_FORMAT(CurDate(),'%Y') " +
                        "       AND" +
                        "       f_month = DATE_FORMAT(CurDate(),'%m')" +
                        "       AND " +
                        "       f_day= DATE_FORMAT(CurDate(),'%d')"+
                        "	) " +
                        "    then 'あり'" +
                        "    else 'なし'"  +
                        "    end  as otw " +
                        " From " +
                        "	t_employee as emp" +
                        "       left outer   Join t_department as dep On emp.f_DepNo = dep.f_DepNo" +
                        "       left outer   Join t_otw as otw On emp.f_empNo = otw.f_empNo" +
                        " where " + 
                        "f_Name Like '%"+empName+ "%'"+
                        " AND dep.f_depName Like '%" +depart+"%'" +
                        " AND f_class Like '%" +className + "%'";
                        
                if(!com.isEmpty()){
                    sql = sql + " AND f_com Like '%" +com + "%'";
                }       
                 
                 sql=sql+" group by emp.f_empNo;";
                 emps = AppempS.getempList(sql, null, null, null, null);
                 comp = AppEmp.getComList();
                 dep = AppEmp.getDepList();
                 System.out.println(sql);

            }else{
            //初回アクセス時  
               System.out.println("index access 1st");
                 emps = AppEmp.getempList();
                 comp = AppEmp.getComList();
                 dep = AppEmp.getDepList();
            } 

            
        %>
	<h1>
		<a href="index.jsp"><!--ロゴ-->
			<img src="images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->
		<div id="login">
			<span id="out"><%=name%>　様</span>
			<a href="login.jsp"><img src="images/logout.gif" id="log_img"></a>
		</div>

	<!--検索-->
	<div id="search">
		
		<div id="easy_but">
			<form action="daytime.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/over_work.gif" value="今日残業する人">
			</form>
			<form action="lastout.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/last_exit.gif" value="最後退室">
			</form>
			<form action="mypage.jsp" method="post" class="slong">
				<input type="image" src="images/mypage.gif" value="マイページ">
			</form>
		</div>
		<form action="index.jsp" method="post" id="search">
			<span id="button">
				検索条件：&nbsp;&nbsp;&nbsp;&nbsp;
		
				名前:<input type="text" name="name" size="20" maxlength="20">

				部署:<select name="place">
					<option value="">-----</option>
                                    <%
                                    for(int i =0;i<dep.size();i++){
                                    %>
					<option value="<%=dep.get(i)%>"><%=dep.get(i)%></option>					
                                    <%
                                    }
                                    %>
				</select>

				雇用形態:<select name="class">
                                    
					<option value="">-----</option>
					<option value="社員">社員</option>
					<option value="準委任">準委任</option>
					<option value="請負">請負</option>
                                        <option value="派遣">派遣</option>
				</select>

				派遣元会社:<select name="com">
                                    <option value="" selected="selected">-----</option>
                                    <%
                                    for(int i =0;i<comp.size();i++){
                                    %>
					<option value="<%=comp.get(i)%>"><%=comp.get(i)%></option>					
                                    <%
                                    }
                                    %>
				</select>

				
				<button type="submit" class="img_but">
					<img src="images/search_19.gif" width="60" height="18">
				</button>
			
				<button type="reset" class="img_but">
  					<img src="images/reset_19.gif">
  				</button>
  			</span>
		</form>


<br />

	<div id="view">
		<span class="text_top"><a href="otime.jsp" >残業一覧</a></span>
	</div>
	
	<div id="sighin">
		<span class="text_top"><a href="otimein.jsp">残業登録</a></span>
	</div>
	
	<div id="notsigh">
		<span class="text_top"><a href="nototime.jsp">残業不可登録</a></span>
	</div>
	

	</div>
	<!--検索終了-->
	
	<!--タグの始まり-->
		<div id="container">
		<!--具体内容-->
			<ul class="panel">
			<!--要員一覧-->
				<li id="tab1">

				<!--要員具体内容-->
				<!--要員一　繰り返し項目-->
				<!--要員一-->
				<div class="category lightgray">
<%
                                            for(int i = 0;i < emps.size();i++){
%>

					<table class="border">
                                                <tr>
                                                    <td rowspan="5"><img src="images/syain1.gif" width="175px" height="200px"alt="社員の写真"></td>
							<td>名前</td>
							<td class="tdright"><%=emps.get(i).get("f_name")%></td>
							<td>部署</td>
							<td class="tdright"><%=emps.get(i).get("f_DepName")%></td>

                                                </tr>
							
						<tr>
							<td>契約形態</td>
							<td class="tdright"><%=emps.get(i).get("f_class")%></td>
							<td>残業</td>
							<td class="tdright"><%=emps.get(i).get("f_otw")%></td>
						</tr>
						<tr>
							<td>メールアドレス</td>
							<td class="tdright" colspan="3"><a href="<%=emps.get(i).get("f_mail")%>"><%=emps.get(i).get("f_mail")%></a></td>
						</tr>
					</table><br />
<%     
                                            }             
%>

				</div>
				<!--要員一完了-->
				<hr />

				</div>
				<!--要員具体内容完了-->
				</li><!--要員一覧終了-->

			</ul><!--具体内容終了-->
		</div><!--タグの終了-->

		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxL">｜<a href="privacy/index.html">個人情報保護方針</a>｜<a href="sitemap/index.html">サイトマップ</a>｜</div>
				<div class="boxR">
					<img src="images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>
	</body>
</html>
