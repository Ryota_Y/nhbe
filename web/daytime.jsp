<%-- 
    Document   : daytime
    Created on : 2015/06/22, 16:34:00
    Author     : win8nhs20249
--%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<title>社員ページ</title>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/jsindex.js"></script>
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" type="text/css" href="css/sugiyama.css">
	</head>



	<body>
	<%
            session = request.getSession(false);
            String name = "none";
            if(!session.isNew()){
                 name = session.getAttribute("f_name").toString();
            }
        
            App.employee AppEmp = new App.employee();
            App.overtime Appotw = new App.overtime();
            ArrayList<HashMap<String,String>> emps = Appotw.getotwList();
            ArrayList<String> comp = AppEmp.getComList();
            ArrayList<String> dep = AppEmp.getDepList();
        %>
	<h1>
		<a href="index.jsp"><!--ロゴ-->
			<img src="images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->

		<div id="login">
			<span id="out"><%=name%>　様</span>
			<a href="login.jsp"><img src="images/logout.gif" id="log_img"></a>
		</div>

	<div id="search">
		
		<div id="easy_but">
			<form action="" method="post" class="slong" id="easy_left">
				<input type="image" src="images/over_work_select.gif" value="今日残業する人">
			</form>
			<form action="lastout.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="images/last_exit.gif" value="最後退室">
			</form>
			<form action="mypage.jsp" method="post" class="slong">
				<input type="image" src="images/mypage.gif" value="マイページ">
			</form>
		</div>

<br />

	<div id="view">
		<span class="text_top"><a href="otime.jsp" >残業一覧</a></span>
	</div>
	
	<div id="sighin">
		<span class="text_top"><a href="otimein.jsp">残業登録</a></span>
	</div>
	
	<div id="notsigh">
		<span class="text_top"><a href="nototime.jsp">残業不可登録</a></span>
	</div>
	

	</div>
	
	<!--タグの始まり-->
		<div id="container">
		<!--具体内容-->
			<ul class="panel">
			<!--要員一覧-->
				<li id="tab1">
				<br />
				<!--要員具体内容-->
				<!--要員一　繰り返し項目-->
				<!--要員一-->
<!--要員一-->
				<div class="category lightgray">
<%
                                        if(!emps.isEmpty()){
                                            
                                            for(int i = 0;i < emps.size();i++){
%>

					<table class="border">
                                                <tr>
                                                    <td rowspan="5"><img src="images/syain1.gif" width="175px" height="200px"alt="社員の写真"></td>
							<td>名前</td>
							<td class="tdright"><%=emps.get(i).get("f_name")%></td>
							<td>部署</td>
							<td class="tdright"><%=emps.get(i).get("f_DepName")%></td>

                                                </tr>
							
						<tr>
							<td>契約形態</td>
							<td class="tdright"><%=emps.get(i).get("f_class")%></td>
							<td>残業</td>
							<td class="tdright"><%=emps.get(i).get("f_otw")%></td>
						</tr>
						<tr>
							<td>メールアドレス</td>
							<td class="tdright" colspan="3"><a href="<%=emps.get(i).get("f_mail")%>"><%=emps.get(i).get("f_mail")%></a></td>
						</tr>
					</table><br />
<%     
                                            }             
                                        }else{
%>
                                        <p>本日の残業者は現在0人です。</p>
<%
                                        }
%>

				</div>
				<!--要員一完了-->
				<hr />


				<!--要員具体内容完了-->

				</li><!--要員一覧終了-->

			</ul><!--具体内容終了-->
		</div><!--タグの終了-->

		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxR">
					<img src="images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>
	</body>
</html>