Select 
	emp.f_empNo,
	emp.f_name,
    dep.f_DepName,
    emp.f_class,
    emp.f_mail,
    emp.f_image,
    case
    when 
		otw.f_empNo = ( select 
							f_empNo
						from
							t_otw
						where
						f_date = now()
						) 
    then 'あり'
    else 'なし'
  end
From 
	t_employee as emp
    Join t_department as dep On emp.f_DepNo = dep.f_DepNo
    Join t_otw as otw On emp.f_empNo = otw.f_empNo
group by emp.f_empNo
;