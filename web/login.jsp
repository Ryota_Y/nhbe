<%-- 
    Document   : login
    Created on : 2015/06/05, 15:58:26
    Author     : win8nhs20249
--%>

<%@page import="Auth.AuthLogin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />
		<title>一般社員ログインページ</title>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		<script src="js/jquery.darktooltip.js"></script>
		<script type="text/javascript" src="js/calendar.js"></script>
		<script type="text/javascript" src="js/long.js"></script>
		<link rel="stylesheet" href="css/darktooltip.css">
		<link rel="stylesheet" type="text/css" href="css/common.css">
		<link rel="stylesheet" href="css/sugiyama.css">

	</head>
<%

    if(request.getMethod().equals("POST")){
            String employee_num = request.getParameter("empNo"),
            password     = request.getParameter("Pass");
            
            System.out.println(employee_num);
            System.out.println(password);
            
            //認証処理
            Auth.AuthLogin auth = new AuthLogin();
            if(auth.login(employee_num, password , request)){
                System.out.println("true");
//                 String empNo=(String)session.getAttribute("f_empNo");
//                 System.out.println(empNo);
            %>
                <jsp:forward page="index.jsp" />
            <%
                }else{
                System.out.println("false");
            }
            
    }else{
            System.out.println("初回アクセス");        
    } 


    
    
%>
    
    
	<body>
	
	<h1>
		<a href="#"><!--ロゴ-->
			<img src="images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->

	<!--タグの始まり-->
		<div id="container">
		<!--具体内容-->
			<ul class="panel">

				<!--残業登録画面カレンダー形式-->
				<li id="tab3">
				<div id="login_panel">
					<form  method="post">
						<span id="login_title">社員番号とパスワードを入力ください。</span><br />
						<span id="employee_num">社員番号　：<input type="text" name="empNo"></span><br />
						<span id="employee_pass">パスワード：</span><input type="password" name="Pass"></span><br />
						<span id="login_button">
							<input type="submit" value="ログイン" id="login_submit">
							<input type="reset" value="リセット" id="login_reset">
						</span>
					</form>

				</div>

				</li><!--登録ページ覧終了-->
	
			</ul><!--具体内容終了-->
		</div><!--タグの終了-->

		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxL">｜<a href="privacy/index.html">個人情報保護方針</a>｜<a href="sitemap/index.html">サイトマップ</a>｜</div>
				<div class="boxR">
					<img src="images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>
	</body>
</html>