﻿-- MySQL dump 10.13  Distrib 5.1.46, for Win32 (ia32)
--
-- Host: localhost    Database: nhbe
-- ------------------------------------------------------
-- Server version	5.1.46-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_card`
--

DROP TABLE IF EXISTS `t_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_card` (
  `f_cardNo` varchar(6) NOT NULL COMMENT 'カードNo',
  `f_distribute` date DEFAULT NULL COMMENT '配布日',
  `f_collect` date DEFAULT NULL COMMENT '回収日',
  PRIMARY KEY (`f_cardNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='カード情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_card`
--

LOCK TABLES `t_card` WRITE;
/*!40000 ALTER TABLE `t_card` DISABLE KEYS */;
INSERT INTO `t_card` VALUES ('1234','2015-01-01',NULL),('1235','2015-01-02',NULL),('1236','2015-01-03',NULL),('1237','2015-01-04',NULL),('1238','2015-01-05',NULL),('1239','2015-01-06',NULL),('1240','2015-01-07',NULL),('1241','2015-01-08',NULL),('1242','2015-01-09',NULL),('1243','2015-01-10',NULL),('1244','2015-01-11',NULL),('1245','2015-01-12',NULL),('1246','2015-01-13',NULL),('1247','2015-01-14',NULL),('1248','2015-01-15',NULL),('1249','2015-01-16',NULL),('1250','2015-01-17',NULL),('1251','2015-01-18',NULL),('1252','2015-01-19',NULL),('1253','2015-01-20',NULL),('1254','2015-01-21',NULL),('1255','2015-01-22',NULL),('1256','2015-01-23',NULL),('1257','2015-01-24',NULL),('1258','2015-01-25',NULL),('1259','2015-01-26',NULL),('1260','2015-01-27',NULL),('1261','2015-01-28',NULL),('1262','2015-01-29',NULL),('1263','2015-01-30',NULL);
/*!40000 ALTER TABLE `t_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_department`
--

DROP TABLE IF EXISTS `t_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_department` (
  `f_DepNo` char(4) NOT NULL COMMENT '事業所CD',
  `f_DepName` varchar(20) NOT NULL COMMENT '事業所名',
  `f_GroupName` varchar(20) DEFAULT NULL COMMENT 'グループ名',
  PRIMARY KEY (`f_DepNo`),
  UNIQUE KEY `f_DepNo` (`f_DepNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='所属';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_department`
--

LOCK TABLES `t_department` WRITE;
/*!40000 ALTER TABLE `t_department` DISABLE KEYS */;
INSERT INTO `t_department` VALUES ('2771','ITサービス事業部','海外支援'),('2772','ITサービス事業部','加工企画'),('2773','ITサービス事業部','システム企画'),('2774','ITサービス事業部','食肉事業'),('2775','ITサービス事業部','加工開発'),('2776','ITサービス事業部','加工運用'),('2777','ITサービス事業部','品質保証'),('2778','ITサービス事業部','関連企業'),('2779','ITサービス事業部','経営見える化'),('2780','ITサービス事業部','Web・情報系'),('2781','ITサービス事業部','EDI開発'),('2782','ITサービス事業部','会計・人事'),('2783','ITサービス事業部','インフラ・共通系'),('2784','ITサービス事業部','インフラ企画'),('2785','ITサービス事業部','ビジネスサポート'),('2786','ITサービス事業部','クオリティサービス'),('2787','経理事業部','食肉東京'),('2788','経理事業部','東日本加工事業'),('2789','経理事業部','東日本営業'),('2790','経理事業部','本社・食肉大阪'),('2791','経理事業部','西日本営業・資金'),('2792','事業統括部','事業管理'),('2793','事業統括部','人事管理');
/*!40000 ALTER TABLE `t_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_employee`
--

DROP TABLE IF EXISTS `t_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_employee` (
  `f_empNo` varchar(6) NOT NULL COMMENT '要員No : 期-連番',
  `f_name` varchar(15) NOT NULL COMMENT '名前',
  `f_nameKana` varchar(30) NOT NULL COMMENT 'ナマエ',
  `f_place` varchar(5) NOT NULL COMMENT '勤務地',
  `f_AD` varchar(9) NOT NULL COMMENT 'AD',
  `f_status` char(4) NOT NULL DEFAULT '在籍' COMMENT '在籍状況 : 在籍　or　終了',
  `f_class` varchar(3) NOT NULL COMMENT '社員区分 : 派遣、請負、準委任、社員',
  `f_EID` varchar(15) DEFAULT NULL COMMENT '社員番号',
  `f_position` char(3) NOT NULL COMMENT '役職 : 一般職 or 管理職\n',
  `f_DepNo` char(4) NOT NULL COMMENT '事業所CD',
  `f_cardNo` varchar(6) DEFAULT NULL COMMENT 'カードNo',
  `f_com` varchar(30) DEFAULT NULL COMMENT '協力会社名',
  `f_start` date DEFAULT NULL COMMENT '在籍開始日 : YYYY/MM/DD\n',
  `f_end` date DEFAULT NULL COMMENT '在籍終了日 : YYYY/MM/DD\n',
  `f_image` varchar(20) DEFAULT NULL COMMENT '画像名',
  `f_ringi` varchar(20) DEFAULT NULL COMMENT '稟議No',
  `f_contract` date DEFAULT NULL COMMENT '契約締結日',
  `f_pass` varchar(15) NOT NULL COMMENT 'パスワード',
  `f_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最終更新日 : YYYY/MM/DD\n',
  PRIMARY KEY (`f_empNo`),
  UNIQUE KEY `f_empNo` (`f_empNo`),
  KEY `f_cardNo` (`f_cardNo`),
  KEY `f_DepNo` (`f_DepNo`),
  CONSTRAINT `t_employee_ibfk_2` FOREIGN KEY (`f_DepNo`) REFERENCES `t_department` (`f_DepNo`),
  CONSTRAINT `t_employee_ibfk_1` FOREIGN KEY (`f_cardNo`) REFERENCES `t_card` (`f_cardNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='要員情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_employee`
--

LOCK TABLES `t_employee` WRITE;
/*!40000 ALTER TABLE `t_employee` DISABLE KEYS */;
INSERT INTO `t_employee` VALUES ('31_001','田尻 光','タジリ ヒカル','大阪','UH1234567','在籍','社員','','一般職','2771','1234',NULL,'2015-01-01',NULL,NULL,'丙-15-NHBE-1234','2015-01-01','tdgB','2000-01-01 03:00:00'),('31_002','神保 麻緒','ジンボ マオ','大阪','UH1234568','在籍','社員','','一般職','2772','1235',NULL,'2015-01-02',NULL,NULL,'丙-15-NHBE-1235','2015-01-02','nFsv','2000-01-02 03:00:00'),('31_003','浜 さゆり','ハマ サユリ','大阪','UH1234569','在籍','社員','','一般職','2773','1236',NULL,'2015-01-03',NULL,NULL,'丙-15-NHBE-1236','2015-01-03','S0vz','2000-01-03 03:00:00'),('31_004','神原 了','カンバラ リョウ','大阪','UH1234570','在籍','社員','','一般職','2774','1237',NULL,'2015-01-04',NULL,NULL,'丙-15-NHBE-1237','2015-01-04','yq5j','2000-01-04 03:00:00'),('31_005','結城 寧',' ユウキ ネイ','東京','UH1234571','在籍','社員','','一般職','2775','1238',NULL,'2015-01-05',NULL,NULL,'丙-15-NHBE-1238','2015-01-05','u6ml','2000-01-05 03:00:00'),('31_006','岡田 杏','オカダ アン','大阪','UH1234572','在籍','社員','','一般職','2776','1239',NULL,'2015-01-06',NULL,NULL,'丙-15-NHBE-1239','2015-01-06','0td8','2000-01-06 03:00:00'),('31_007','浜本 花','ハマモト ハナ','大阪','UH1234573','在籍','社員','','一般職','2777','1240',NULL,'2015-01-07',NULL,NULL,'丙-15-NHBE-1240','2015-01-07','Ncyb','2000-01-07 03:00:00'),('31_008','西村 幸平','ニシムラ コウヘイ','大阪','UH1234574','在籍','社員','','一般職','2778','1241',NULL,'2015-01-08',NULL,NULL,'丙-15-NHBE-1241','2015-01-08','99W5','2000-01-08 03:00:00'),('31_009','増田 倫子','マスダ ノリコ','大阪','UH1234575','在籍','社員','','一般職','2779','1242',NULL,'2015-01-09',NULL,NULL,'丙-15-NHBE-1242','2015-01-09','lNf9','2000-01-09 03:00:00'),('31_010','後藤 優','ゴトウ ユウ','大阪','UH1234576','在籍','社員','','一般職','2780','1243',NULL,'2015-01-10',NULL,NULL,'丙-15-NHBE-1243','2015-01-10','by0Z','2000-01-10 03:00:00'),('31_011','坂本 扶樹','サカモト モトキ','大阪','UH1234577','在籍','社員','','一般職','2781','1244',NULL,'2015-01-11',NULL,NULL,'丙-15-NHBE-1244','2015-01-11','7pt5','2000-01-11 03:00:00'),('31_012','小泉 晃司','コイズミ コウジ','大阪','UH1234578','在籍','社員','','一般職','2782','1245',NULL,'2015-01-12',NULL,NULL,'丙-15-NHBE-1245','2015-01-12','SjLY','2000-01-12 03:00:00'),('31_013','長崎 夏希','ナガサキ ナツキ','大阪','UH1234579','在籍','社員','','一般職','2783','1246',NULL,'2015-01-13',NULL,NULL,'丙-15-NHBE-1246','2015-01-13','d0L7','2000-01-13 03:00:00'),('31_014','茂木 雅之','モギ マサユキ','大阪','UH1234580','在籍','社員','','一般職','2784','1247',NULL,'2015-01-14',NULL,NULL,'丙-15-NHBE-1247','2015-01-14','eOTY','2000-01-14 03:00:00'),('31_015','陸奥守吉行','ムツノカミヨシユキ','大阪','UH1234581','在籍','社員','','一般職','2785','1248',NULL,'2015-01-15',NULL,NULL,'丙-15-NHBE-1248','2015-01-15','JNyq','2000-01-15 03:00:00'),('31_016','石丸 草太','イシマル ソウタ','東京','UH1234582','在籍','社員','','一般職','2786','1249',NULL,'2015-01-16',NULL,NULL,'丙-15-NHBE-1249','2015-01-16','NoDJ','2000-01-16 03:00:00'),('31_017','梶原 窈','カジワラ ヨウ','東京','UH1234583','在籍','社員','','一般職','2787','1250',NULL,'2015-01-17',NULL,NULL,'丙-15-NHBE-1250','2015-01-17','tcQ2','2000-01-17 03:00:00'),('31_018','神野 博之','カミノ ヒロユキ','東京','UH1234584','在籍','社員','','一般職','2788','1251',NULL,'2015-01-18',NULL,NULL,'丙-15-NHBE-1251','2015-01-18','oOA5','2000-01-18 03:00:00'),('31_019','板倉 千佳子','イタクラ チカコ','東京','UH1234585','在籍','社員','','一般職','2789','1252',NULL,'2015-01-19',NULL,NULL,'丙-15-NHBE-1252','2015-01-19','Y7Yb','2000-01-19 03:00:00'),('31_020','藤岡 明','フジオカ アキラ','東京','UH1234586','在籍','社員','','一般職','2790','1253',NULL,'2015-01-20',NULL,NULL,'丙-15-NHBE-1253','2015-01-20','ufVW','2000-01-20 03:00:00'),('31_021','志水 法嗣','シミズ ホウシ','東京','UH1234587','在籍','社員','','一般職','2791','1254',NULL,'2015-01-21',NULL,NULL,'丙-15-NHBE-1254','2015-01-21','uhpx','2000-01-21 03:00:00'),('31_022','宇都宮 憲一','ウツノミヤ ケンイチ','東京','UH1234588','在籍','社員','','一般職','2792','1255',NULL,'2015-01-22',NULL,NULL,'丙-15-NHBE-1255','2015-01-22','Pxdx','2000-01-22 03:00:00'),('31_023','藤村 兼','フジムラ ケン','東京','UH1234589','在籍','社員','','一般職','2793','1256',NULL,'2015-01-23',NULL,NULL,'丙-15-NHBE-1256','2015-01-23','VazN','2000-01-23 03:00:00'),('31_024','平 彩華','タイラ アヤカ','東京','UH1234590','在籍','派遣','','一般職','2771','1257','(株)平田グローバルネット','2015-01-24',NULL,NULL,'丙-15-NHBE-1257','2015-01-24','wxi6','2000-01-24 03:00:00'),('31_025','越智 美優','オチ ミュウ','東京','UH1234591','在籍','派遣','','一般職','2772','1258','(株)BCDシステム','2015-01-25',NULL,NULL,'丙-15-NHBE-1258','2015-01-25','tvvd','2000-01-25 03:00:00'),('31_026','森岡 拓郎','モリオカ タクロウ','東京','UH1234592','在籍','派遣','','一般職','2773','1259','(株)ABCマネジメント','2015-01-26',NULL,NULL,'丙-15-NHBE-1259','2015-01-26','Yu7q','2000-01-26 03:00:00'),('31_027','早美 ひろみ','ハヤミ ヒロミ','東京','UH1234593','在籍','派遣','','一般職','2774','1260','(株)ABCマネジメント','2015-01-27',NULL,NULL,'丙-15-NHBE-1260','2015-01-27','3Igo','2000-01-27 03:00:00'),('31_028','和田 夏空','ワダ ソラ','東京','UH1234594','在籍','派遣','','一般職','2775','1261','中嶋サービス(株)','2015-01-28',NULL,NULL,'丙-15-NHBE-1261','2015-01-28','VU6W','2000-01-28 03:00:00'),('31_029','平田 章語','ヒラタ ショウゴ','東京','UH1234595','在籍','派遣','','一般職','2776','1262','(株)平田グローバルネット','2015-01-29',NULL,NULL,'丙-15-NHBE-1262','2015-01-29','PrSG','2000-01-29 03:00:00'),('31_030','藤村 れいな','フジムラ レイナ','東京','UH1234596','在籍','派遣','','一般職','2777','1263','益子マネジメント(株)','2015-01-30',NULL,NULL,'丙-15-NHBE-1263','2015-01-30','lTXS','2000-01-30 03:00:00');
/*!40000 ALTER TABLE `t_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_lease`
--

DROP TABLE IF EXISTS `t_lease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_lease` (
  `f_ITNo` varchar(9) NOT NULL COMMENT 'IT機器No',
  `f_lease` varchar(11) DEFAULT NULL COMMENT 'リース番号',
  `f_start` date DEFAULT NULL COMMENT '貸出開始日',
  `f_end` date DEFAULT NULL COMMENT '貸出終了日',
  `f_serial` varchar(11) NOT NULL COMMENT 'シリアルコード',
  `f_empNo` varchar(6) NOT NULL COMMENT '要員No : 期-連番',
  PRIMARY KEY (`f_ITNo`),
  UNIQUE KEY `f_empNo` (`f_empNo`),
  CONSTRAINT `t_lease_ibfk_1` FOREIGN KEY (`f_empNo`) REFERENCES `t_employee` (`f_empNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='貸出情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_lease`
--

LOCK TABLES `t_lease` WRITE;
/*!40000 ALTER TABLE `t_lease` DISABLE KEYS */;
INSERT INTO `t_lease` VALUES ('000000001','G210F148000','2000-01-01',NULL,'SN12345678H','31_001'),('000000002','G210F148001','2000-01-02',NULL,'SN12345679H','31_002'),('000000003','G210F148002','2000-01-03',NULL,'SN12345680H','31_003'),('000000004','G210F148003','2000-01-04',NULL,'SN12345681H','31_004'),('000000005','G210F148004','2000-01-05',NULL,'SN12345682H','31_005'),('000000006','G210F148005','2000-01-06',NULL,'SN12345683H','31_006'),('000000007','G210F148006','2000-01-07',NULL,'SN12345684H','31_007'),('000000008','G210F148007','2000-01-08',NULL,'SN12345685H','31_008'),('000000009','G210F148008','2000-01-09',NULL,'SN12345686H','31_009'),('000000010','G210F148009','2000-01-10',NULL,'SN12345687H','31_010');
/*!40000 ALTER TABLE `t_lease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_nototw`
--

DROP TABLE IF EXISTS `t_nototw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_nototw` (
  `f_nototw` date NOT NULL COMMENT '残業不可年月日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='残業禁止日';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_nototw`
--

LOCK TABLES `t_nototw` WRITE;
/*!40000 ALTER TABLE `t_nototw` DISABLE KEYS */;
INSERT INTO `t_nototw` VALUES ('2015-05-02'),('2015-05-09'),('2015-05-16'),('2015-05-23');
/*!40000 ALTER TABLE `t_nototw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_otw`
--

DROP TABLE IF EXISTS `t_otw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_otw` (
  `f_empNo` varchar(6) NOT NULL COMMENT '要員No : 期-連番',
  `f_date` date NOT NULL COMMENT '残業日',
  `f_start` time NOT NULL COMMENT '残業開始時刻',
  `f_end` time NOT NULL COMMENT '残業終了時刻',
  `f_accept` tinyint(1) NOT NULL DEFAULT '0' COMMENT '承認 : true or false',
  KEY `f_empNo` (`f_empNo`),
  CONSTRAINT `t_otw_ibfk_1` FOREIGN KEY (`f_empNo`) REFERENCES `t_employee` (`f_empNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='残業申請';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_otw`
--

LOCK TABLES `t_otw` WRITE;
/*!40000 ALTER TABLE `t_otw` DISABLE KEYS */;
INSERT INTO `t_otw` VALUES ('31_001','2000-01-01','18:00:00','20:00:00',0),('31_002','2000-01-02','18:00:00','22:00:00',0),('31_003','2000-01-03','18:00:00','20:00:00',0),('31_004','2000-01-04','18:00:00','22:00:00',0),('31_005','2000-01-05','18:00:00','20:00:00',0),('31_006','2000-01-06','18:00:00','22:30:00',0),('31_007','2000-01-07','18:00:00','20:00:00',0),('31_008','2000-01-08','18:00:00','22:30:00',0),('31_009','2000-01-09','18:00:00','23:00:00',0),('31_010','2000-01-10','18:00:00','20:00:00',0);
/*!40000 ALTER TABLE `t_otw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_seat`
--

DROP TABLE IF EXISTS `t_seat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_seat` (
  `f_seatmap` char(2) NOT NULL COMMENT '座席 : A1',
  `f_empNo` varchar(6) NOT NULL COMMENT '要員No : 期-連番',
  `f_tel` varchar(8) DEFAULT NULL COMMENT '内線',
  `t_outtel` varchar(10) DEFAULT NULL COMMENT '外線',
  PRIMARY KEY (`f_seatmap`),
  UNIQUE KEY `f_empNo` (`f_empNo`),
  CONSTRAINT `t_seat_ibfk_1` FOREIGN KEY (`f_empNo`) REFERENCES `t_employee` (`f_empNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='座席表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_seat`
--

LOCK TABLES `t_seat` WRITE;
/*!40000 ALTER TABLE `t_seat` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_seat` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-08 12:44:44

ALTER TABLE t_employee ADD f_mail varchar(50) NOT NULL DEFAULT 'none';