SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS t_seat;
DROP TABLE IF EXISTS t_OTW;
DROP TABLE IF EXISTS t_lease;
DROP TABLE IF EXISTS t_employee;
DROP TABLE IF EXISTS t_card;
DROP TABLE IF EXISTS t_Department;
DROP TABLE IF EXISTS t_notOTW;




/* Create Tables */

CREATE TABLE t_card
(
	f_cardNo varchar(6) NOT NULL COMMENT 'カードNo',
	f_distribute date COMMENT '配布日',
	f_collect date COMMENT '回収日',
	PRIMARY KEY (f_cardNo)
) COMMENT = 'カード情報';


CREATE TABLE t_Department
(
	f_DepNo char(4) NOT NULL UNIQUE COMMENT '事業所CD',
	f_DepName varchar(20) NOT NULL COMMENT '事業所名',
	f_GroupName varchar(20) COMMENT 'グループ名',
	PRIMARY KEY (f_DepNo)
) COMMENT = '所属';


CREATE TABLE t_employee
(
	-- 期-連番
	f_empNo varchar(6) NOT NULL UNIQUE COMMENT '要員No : 期-連番',
	f_name varchar(15) NOT NULL COMMENT '名前',
	f_nameKana varchar(30) NOT NULL COMMENT 'ナマエ',
	f_place varchar(5) NOT NULL COMMENT '勤務地',
	f_AD varchar(9) NOT NULL COMMENT 'AD',
	-- 在籍　or　終了
	f_status char(4) DEFAULT '在籍' NOT NULL COMMENT '在籍状況 : 在籍　or　終了',
	-- 派遣、請負、準委任、社員
	f_class varchar(3) NOT NULL COMMENT '社員区分 : 派遣、請負、準委任、社員',
	f_EID varchar(15) COMMENT '社員番号',
	-- 一般職 or 管理職
	-- 
	f_position char(3) NOT NULL COMMENT '役職 : 一般職 or 管理職
',
	f_DepNo char(4) NOT NULL UNIQUE COMMENT '事業所CD',
	f_cardNo varchar(6) COMMENT 'カードNo',
	f_com varchar(30) COMMENT '協力会社名',
	-- YYYY/MM/DD
	-- 
	f_start date COMMENT '在籍開始日 : YYYY/MM/DD
',
	-- YYYY/MM/DD
	-- 
	f_end date COMMENT '在籍終了日 : YYYY/MM/DD
',
	f_image varchar(20) COMMENT '画像名',
	f_ringi varchar(20) COMMENT '稟議No',
	f_contract date COMMENT '契約締結日',
	f_pass varchar(15) NOT NULL COMMENT 'パスワード',
	-- YYYY/MM/DD
	-- 
	f_update timestamp NOT NULL COMMENT '最終更新日 : YYYY/MM/DD
',
	PRIMARY KEY (f_empNo)
) COMMENT = '要員情報';


CREATE TABLE t_lease
(
	f_ITNo varchar(9) NOT NULL COMMENT 'IT機器No',
	f_lease varchar(11) COMMENT 'リース番号',
	f_start date COMMENT '貸出開始日',
	f_end date COMMENT '貸出終了日',
	f_serial varchar(11) NOT NULL COMMENT 'シリアルコード',
	-- 期-連番
	f_empNo varchar(6) NOT NULL UNIQUE COMMENT '要員No : 期-連番',
	PRIMARY KEY (f_ITNo)
) COMMENT = '貸出情報';


CREATE TABLE t_notOTW
(
	f_nototw date NOT NULL COMMENT '残業不可年月日'
) COMMENT = '残業禁止日';


CREATE TABLE t_OTW
(
	-- 期-連番
	f_empNo varchar(6) NOT NULL COMMENT '要員No : 期-連番',
	f_date date NOT NULL COMMENT '残業日',
	f_start time NOT NULL COMMENT '残業開始時刻',
	f_end time NOT NULL COMMENT '残業終了時刻',
	-- true or false
	f_accept boolean DEFAULT '0' NOT NULL COMMENT '承認 : true or false'
) COMMENT = '残業申請';


CREATE TABLE t_seat
(
	-- A1
	f_seatmap char(2) NOT NULL COMMENT '座席 : A1',
	-- 期-連番
	f_empNo varchar(6) NOT NULL UNIQUE COMMENT '要員No : 期-連番',
	f_tel varchar(8) COMMENT '内線',
	t_outtel varchar(10) COMMENT '外線',
	PRIMARY KEY (f_seatmap)
) COMMENT = '座席表';



/* Create Foreign Keys */

ALTER TABLE t_employee
	ADD FOREIGN KEY (f_cardNo)
	REFERENCES t_card (f_cardNo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE t_employee
	ADD FOREIGN KEY (f_DepNo)
	REFERENCES t_Department (f_DepNo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE t_seat
	ADD FOREIGN KEY (f_empNo)
	REFERENCES t_employee (f_empNo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE t_OTW
	ADD FOREIGN KEY (f_empNo)
	REFERENCES t_employee (f_empNo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE t_lease
	ADD FOREIGN KEY (f_empNo)
	REFERENCES t_employee (f_empNo)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



