package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.HashMap;
import java.util.ArrayList;

public final class daytime_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0\r\n");
      out.write(" Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n");
      out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"ja\" lang=\"ja\">\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\r\n");
      out.write("\t\t<meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />\r\n");
      out.write("\t\t<meta http-equiv=\"Content-Script-Type\" content=\"text/javascript\" />\r\n");
      out.write("\t\t<title>社員ページ</title>\r\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"js/jquery-1.11.2.min.js\"></script>\r\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"js/jquery.js\"></script>\r\n");
      out.write("\t\t<script type=\"text/javascript\" src=\"js/jsindex.js\"></script>\r\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/common.css\">\r\n");
      out.write("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/sugiyama.css\">\r\n");
      out.write("\t</head>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t<body>\r\n");
      out.write("\t");

            session = request.getSession(false);
            String name = "none";
            if(!session.isNew()){
                 name = session.getAttribute("f_name").toString();
            }
        
            App.employee AppEmp = new App.employee();
            App.overtime Appotw = new App.overtime();
            ArrayList<HashMap<String,String>> emps = Appotw.getotwList();
            ArrayList<String> comp = AppEmp.getComList();
            ArrayList<String> dep = AppEmp.getDepList();
        
      out.write("\r\n");
      out.write("\t<h1>\r\n");
      out.write("\t\t<a href=\"index.html\"><!--ロゴ-->\r\n");
      out.write("\t\t\t<img src=\"images/logo.gif\" alt=\"日本ハムビジネスエキスパート\">\r\n");
      out.write("\t\t</a>\r\n");
      out.write("\t</h1><!--ロゴ終了-->\r\n");
      out.write("\r\n");
      out.write("\t\t<div id=\"login\">\r\n");
      out.write("\t\t\t<span id=\"out\">");
      out.print(name);
      out.write("　様</span>\r\n");
      out.write("\t\t\t<a href=\"login.html\"><img src=\"images/logout.gif\" id=\"log_img\"></a>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t<div id=\"search\">\r\n");
      out.write("\t\t\r\n");
      out.write("\t\t<div id=\"easy_but\">\r\n");
      out.write("\t\t\t<form action=\"\" method=\"post\" class=\"slong\" id=\"easy_left\">\r\n");
      out.write("\t\t\t\t<input type=\"image\" src=\"images/over_work_select.gif\" value=\"今日残業する人\">\r\n");
      out.write("\t\t\t</form>\r\n");
      out.write("\t\t\t<form action=\"lastout.html\" method=\"post\" class=\"slong\" id=\"easy_left\">\r\n");
      out.write("\t\t\t\t<input type=\"image\" src=\"images/last_exit.gif\" value=\"最後退室\">\r\n");
      out.write("\t\t\t</form>\r\n");
      out.write("\t\t\t<form action=\"mypage.html\" method=\"post\" class=\"slong\">\r\n");
      out.write("\t\t\t\t<input type=\"image\" src=\"images/mypage.gif\" value=\"マイページ\">\r\n");
      out.write("\t\t\t</form>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\r\n");
      out.write("<br />\r\n");
      out.write("\r\n");
      out.write("\t<div id=\"view\">\r\n");
      out.write("\t\t<span class=\"text_top\"><a href=\"otime.html\" >残業一覧</a></span>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<div id=\"sighin\">\r\n");
      out.write("\t\t<span class=\"text_top\"><a href=\"otimein.html\">残業登録</a></span>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<div id=\"notsigh\">\r\n");
      out.write("\t\t<span class=\"text_top\"><a href=\"nototime.html\">残業不可登録</a></span>\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\r\n");
      out.write("\t</div>\r\n");
      out.write("\t\r\n");
      out.write("\t<!--タグの始まり-->\r\n");
      out.write("\t\t<div id=\"container\">\r\n");
      out.write("\t\t<!--具体内容-->\r\n");
      out.write("\t\t\t<ul class=\"panel\">\r\n");
      out.write("\t\t\t<!--要員一覧-->\r\n");
      out.write("\t\t\t\t<li id=\"tab1\">\r\n");
      out.write("\t\t\t\t<br />\r\n");
      out.write("\t\t\t\t<!--要員具体内容-->\r\n");
      out.write("\t\t\t\t<!--要員一　繰り返し項目-->\r\n");
      out.write("\t\t\t\t<!--要員一-->\r\n");
      out.write("<!--要員一-->\r\n");
      out.write("\t\t\t\t<div class=\"category lightgray\">\r\n");

                                        if(!emps.isEmpty()){
                                            
                                            for(int i = 0;i < emps.size();i++){

      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t<table class=\"border\">\r\n");
      out.write("                                                <tr>\r\n");
      out.write("                                                    <td rowspan=\"5\"><img src=\"images/syain1.gif\" width=\"175px\" height=\"200px\"alt=\"社員の写真\"></td>\r\n");
      out.write("\t\t\t\t\t\t\t<td>名前</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td class=\"tdright\">");
      out.print(emps.get(i).get("f_name"));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td>部署</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td class=\"tdright\">");
      out.print(emps.get(i).get("f_DepName"));
      out.write("</td>\r\n");
      out.write("\r\n");
      out.write("                                                </tr>\r\n");
      out.write("\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t\t<td>契約形態</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td class=\"tdright\">");
      out.print(emps.get(i).get("f_class"));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td>残業</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td class=\"tdright\">");
      out.print(emps.get(i).get("f_otw"));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t\t<td>メールアドレス</td>\r\n");
      out.write("\t\t\t\t\t\t\t<td class=\"tdright\" colspan=\"3\"><a href=\"");
      out.print(emps.get(i).get("f_mail"));
      out.write('"');
      out.write('>');
      out.print(emps.get(i).get("f_mail"));
      out.write("</a></td>\r\n");
      out.write("\t\t\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t\t</table><br />\r\n");
     
                                            }             
                                        }else{

      out.write("\r\n");
      out.write("                                        <p>本日の残業者は現在0人です。</p>\r\n");

                                        }

      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t<!--要員一完了-->\r\n");
      out.write("\t\t\t\t<hr />\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!--要員具体内容完了-->\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t</li><!--要員一覧終了-->\r\n");
      out.write("\r\n");
      out.write("\t\t\t</ul><!--具体内容終了-->\r\n");
      out.write("\t\t</div><!--タグの終了-->\r\n");
      out.write("\r\n");
      out.write("\t\t<!--footer-->\r\n");
      out.write("\t\t<div id=\"footer\">\r\n");
      out.write("\t\t\t<div class=\"boxC\">\r\n");
      out.write("\t\t\t\t<div class=\"boxR\">\r\n");
      out.write("\t\t\t\t\t<img src=\"images/footer_copy.gif\" alt=\"Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved.\" class=\"fl mt3\">\r\n");
      out.write("\t\t\t\t\t<a href=\"http://www.nipponham.co.jp/group/\" target=\"_blank\"><img src=\"images/footer_nh.gif\" alt=\"Nippon Ham Group\" class=\"fr\"></a>\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\t\t  </div>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
