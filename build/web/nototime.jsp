<%-- 
    Document   : nototime
    Created on : 2015/06/27, 14:32:15
    Author     : nhs20018
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta http-equiv="Content-Style-Type" content="text/css" />
            <meta http-equiv="Content-Script-Type" content="text/javascript" />
            <title>残業不可日登録ページ</title>
            <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.11.2.min.js"></script>
            <script src="<%= request.getContextPath() %>/js/jquery.darktooltip.js"></script>
            <script type="text/javascript" src="<%= request.getContextPath() %>/js/calendar.js"></script>
            <script type="text/javascript" src="<%= request.getContextPath() %>/js/long.js"></script>
            <link rel="stylesheet" href="<%= request.getContextPath() %>/css/darktooltip.css">
            <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/common.css">
            <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/sugiyama.css">
	</head>
	<body>
	
	<h1>
		<a href="<%= request.getContextPath() %>/index.jsp"><!--ロゴ-->
			<img src="<%= request.getContextPath() %>/images/logo.gif" alt="日本ハムビジネスエキスパート">
		</a>
	</h1><!--ロゴ終了-->

		<div id="login">
			<span id="out">山田太郎　様</span>
			<a href="<%= request.getContextPath() %>/login.jsp"><img src="<%= request.getContextPath() %>/images/logout.gif" id="log_img"></a>
		</div>
	<div id="search">
		
		<div id="easy_but">
			<form action="<%= request.getContextPath() %>/daytime.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="<%= request.getContextPath() %>/images/over_work.gif" value="今日残業する人">
			</form>
			<form action="<%= request.getContextPath() %>/lastout.jsp" method="post" class="slong" id="easy_left">
				<input type="image" src="<%= request.getContextPath() %>/images/last_exit.gif" value="最後退室">
			</form>
			<form action="<%= request.getContextPath() %>/mypage.jsp" method="post" class="slong">
				<input type="image" src="<%= request.getContextPath() %>/images/mypage.gif" value="マイページ">
			</form>
		</div>
<br />

	<div id="view">
		<span class="text_top"><a href="<%= request.getContextPath() %>/otime.jsp" >残業一覧</a></span>
	</div>
	
	<div id="sighin">
		<span class="text_top"><a href="<%= request.getContextPath() %>/otimein.jsp">残業登録</a></span>
	</div>
	
	<div id="notsigh">
		<span class="text_top"><a href="<%= request.getContextPath() %>/nototime.jsp">残業不可登録</a></span>
	</div>
	

	</div>
	<!--タグの始まり-->
		<div id="container">
		<!--具体内容-->
			<ul class="panel">

				<!--残業登録画面カレンダー形式-->
				<li id="tab3">
				
				<span id="noto_back"><a href="<%= request.getContextPath() %>/index.jsp">戻る</a></span><br />
				<span>残業不可日管理</span><br />
				<div id="nototime">

                                    <a id="insert">登録</a>
					<form action="<%= request.getContextPath() %>/servlet/nototime" method="post" id="nototime_form">
						日付：
						<select name="year">
							<script type="text/javascript">
							var myD = new Date();
							var myYear = myD.getYear() + 1;
							for(var i=2015;i<2026;i++){
								var sel="";
								if(myYear==i){sel=" selected"}
								document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
								sel="";
							}
							</script>
						</select>年
						<select name="month">
						<script type="text/javascript">
							var myD = new Date();
							var myMonth = myD.getMonth() + 1;
							for(var i=1;i<13;i++){
								var sel="";
								if(myMonth==i){sel=" selected"}
								document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
								sel="";
							}
						</script>
							</select>月

							<select name="day">
								<script type="text/javascript">
								var myD = new Date();
								var myDate = myD.getDate();
							for(var i=1;i<32;i++){
								var sel="";
								if(myDate==i){sel=" selected"}
								document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
								sel="";
							}
						</script>
							</select>日
						<br />
						<span id="button_nottime">
							<button type="submit" class="img_but" id="nototime_submit">
								<img src="<%= request.getContextPath() %>/images/add_19.gif" width="60" height="18">
							</button>
                                                        
							<button type="reset" class="img_but" id="nototime_reset">
  								<img src="<%= request.getContextPath() %>/images/reset_19.gif">
  							</button>
  						</span>
					</form>
				</div>
                                
                                <div id="nototime_del">
                                <a id="delete">削除</a>
                            	<form action="<%= request.getContextPath() %>/servlet/nototime_delete" method="post" id="nototime_form">
						日付：
						<select name="year">
							<script type="text/javascript">
							var myD = new Date();
							var myYear = myD.getYear() + 1;
							for(var i=2015;i<2026;i++){
								var sel="";
								if(myYear==i){sel=" selected"}
								document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
								sel="";
							}
							</script>
						</select>年
						<select name="month">
						<script type="text/javascript">
							var myD = new Date();
							var myMonth = myD.getMonth() + 1;
							for(var i=1;i<13;i++){
								var sel="";
								if(myMonth==i){sel=" selected"}
								document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
								sel="";
							}
						</script>
							</select>月

							<select name="day">
								<script type="text/javascript">
								var myD = new Date();
								var myDate = myD.getDate();
							for(var i=1;i<32;i++){
								var sel="";
								if(myDate==i){sel=" selected"}
								document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
								sel="";
							}
						</script>
							</select>日
						<br />
						<span id="button_nottime">
							<button type="submit" class="img_but" id="nototime_submit">
								<img src="<%= request.getContextPath() %>/images/del_19.gif" width="60" height="18">
							</button>
                                                        
							<button type="reset" class="img_but" id="nototime_reset">
  								<img src="<%= request.getContextPath() %>/images/reset_19.gif">
  							</button>
  						</span>
					</form>
				</div>

				</li><!--登録ページ覧終了-->
	
			</ul><!--具体内容終了-->
		</div><!--タグの終了-->

		<!--footer-->
		<div id="footer">
			<div class="boxC">
				<div class="boxR">
					<img src="<%= request.getContextPath() %>/images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
					<a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="<%= request.getContextPath() %>/images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
				</div>
		  </div>
		</div>
	</body>
</html>
