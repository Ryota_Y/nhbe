function startName() {
			new Suggest.Local(
			"name",    // 入力のエレメントID
			"names", // 補完候補を表示するエリアのID
			s_name,      // 補完候補の検索対象となる配列
			{dispMax: 10, interval: 1000}); // オプション
}
var n_start = function(){new Suggest.Local("name", "names", s_name, {dispMax: 10, highlight: true});};
window.addEventListener ?
window.addEventListener('load', n_start, false) :
window.attachEvent('onload', n_start);

function startCompany() {
			new Suggest.Local(
			"company",    // 入力のエレメントID
			"companys", // 補完候補を表示するエリアのID
			s_company,      // 補完候補の検索対象となる配列
			{dispMax: 10, interval: 1000}); // オプション
}
var c_start = function(){new Suggest.Local("company", "companys", s_company, {dispMax: 10, highlight: true});};
window.addEventListener ?
window.addEventListener('load', c_start, false) :
window.attachEvent('onload', c_start);

function startOffice() {
			new Suggest.Local(
			"office",    // 入力のエレメントID
			"suggest", // 補完候補を表示するエリアのID
			s_office,      // 補完候補の検索対象となる配列
			{dispMax: 10, interval: 1000}); // オプション
}
var o_start = function(){new Suggest.Local("office", "offices", s_office, {dispMax: 10, highlight: true});};
window.addEventListener ?
window.addEventListener('load', o_start, false) :
window.attachEvent('onload', o_start);

function startGroups() {
			new Suggest.Local(
			"groups",    // 入力のエレメントID
			"groupss", // 補完候補を表示するエリアのID
			s_groups,      // 補完候補の検索対象となる配列
			{dispMax: 10, interval: 1000}); // オプション
}
var g_start = function(){new Suggest.Local("groups", "groupss", s_groups, {dispMax: 10, highlight: true});};
window.addEventListener ?
window.addEventListener('load', g_start, false) :
window.attachEvent('onload', g_start);