<%-- 
    Document   : otimein
    Created on : 2015/06/17, 16:55:41
    Author     : nhs20018
--%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0
 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<html>
    <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
	<meta http-equiv="Content-Script-Type" content="text/javascript" />
	<title>残業登録ページ</title>
	<script type="text/javascript" src="<%= request.getContextPath() %>js/jquery-1.11.2.min.js"></script>
	<script src="<%= request.getContextPath() %>/js/jquery.darktooltip.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/js/calendar.js"></script>
	<script type="text/javascript" src="<%= request.getContextPath() %>/js/long.js"></script>
	<link rel="stylesheet" href="<%= request.getContextPath() %>/css/darktooltip.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/common.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/sugiyama.css">
	<link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/cheng.css">
    </head>

    <body>
	
	<h1>
            <a href="<%= request.getContextPath() %>/index.jsp"><!--ロゴ-->
		<img src="<%= request.getContextPath() %>/images/logo.gif" alt="日本ハムビジネスエキスパート">
            </a>
	</h1><!--ロゴ終了-->

	<div id="login">
            <span id="out">山田太郎　様</span>
            <a href="login.jsp"><img src="<%= request.getContextPath() %>/images/logout.gif" id="log_img"></a>
	</div>
	<div id="search">
		
            <div id="easy_but">
		<form action="<%= request.getContextPath() %>/daytime.jsp" method="post" class="slong" id="easy_left">
                	<input type="image" src="<%= request.getContextPath() %>/images/over_work.gif" value="今日残業する人">
		</form>
		<form action="<%= request.getContextPath() %>/lastout.jsp" method="post" class="slong" id="easy_left">
                    <input type="image" src="<%= request.getContextPath() %>/images/last_exit.gif" value="最後退室">
		</form>
		<form action="<%= request.getContextPath() %>/mypage.jsp" method="post" class="slong">
                    <input type="image" src="<%= request.getContextPath() %>/images/mypage.gif" value="マイページ">
		</form>
            </div>
            <form action="#" method="post" id="search">
		<span id="button">
                    検索条件：&nbsp;&nbsp;&nbsp;&nbsp;
		
                    名前:<input type="text" name="name" size="20" maxlength="20">

                    または　部署:<select name="place">
                                    <option value="0">-----</option>
                                    <option value="1">管理部</option>
                                    <option value="2">営業部</option>
                                    <option value="3">開発部</option>
				</select>



				
				<button type="submit" class="img_but">
                                    <img src="<%= request.getContextPath() %>/images/search_19.gif" width="60" height="18">
				</button>
			
				<button type="reset" class="img_but">
                                    <img src="<%= request.getContextPath() %>/images/reset_19.gif">
  				</button>
  		</span>
            </form>


<br />

	<div id="view">
            <span class="text_top"><a href="<%= request.getContextPath() %>/otime.jsp" >残業一覧</a></span>
	</div>
	
	<div id="sighin">
            <span class="text_top"><a href="<%= request.getContextPath() %>/otimein.jsp">残業登録</a></span>
	</div>
	
	<div id="notsigh">
            <span class="text_top"><a href="<%= request.getContextPath() %>/nototime.jsp">残業不可登録</a></span>
	</div>
	

	</div>
	<!--タグの始まり-->
	<div id="container">
            <!--具体内容-->
            <ul class="panel">

		<!--残業登録画面カレンダー形式-->
		<li id="tab3">
                    <span id="otimein_back"><a href="<%= request.getContextPath() %>/index.jsp">戻る</a></span><br />
                    <span>残業登録</span><br />
                    <div id="otimein">
					
                    <form action="<%= request.getContextPath() %>/servlet/otimein" method="post" id="otime_form">
						
			日付：
						
			<select name="year">
                        	<script type="text/javascript">
                                    var myD = new Date();
                                    var myYear = myD.getYear() + 1;
                                    for(var i=2015;i<2026;i++){
					var sel="";
					if(myYear==i){sel=" selected"}
                                        	document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
						sel="";
					}
				</script>
			</select>年
						
			<select name="month">
                            <script type="text/javascript">
				var myD = new Date();
				var myMonth = myD.getMonth() + 1;
				for(var i=1;i<13;i++){
                                    var sel="";
                                    if(myMonth==i){sel=" selected"}
					document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
					sel="";
                                    }
                            </script>
			</select>月

			<select name="day">
                            <script type="text/javascript">
				var myD = new Date();
				var myDate = myD.getDate();
				for(var i=1;i<32;i++){
                                    var sel="";
					if(myDate==i){sel=" selected"}
                                            document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
                                            sel="";
					}
                            </script>
			</select>日<br />
						
            残業開始時間：<select name="time">
                            <script type="text/javascript">
                                var myD = new Date();
				var myHours = myD.getHours();
				for(var i=5;i<24;i++){
                                    var sel="";
                                    if(myHours==i){sel=" selected"}
					document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
					sel="";
                                    }
                            </script>
			</select>時
			<select name="minite">
                            <script type="text/javascript">
                                var myD = new Date();
				var myMinutes = myD.getMinutes();
				for(var i=0;i<61;i+=15){
                                    var sel="";
                                    if(myMinutes<=i&&myMinutes>(i-15)){sel=" selected"}
					document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
					sel="";
                                    }
                            </script>
			</select>分<br />
            残業終了時間：<select name="time_e">
                            <script type="text/javascript">
				var myD = new Date();
				var myHours = myD.getHours();
                                for(var i=5;i<24;i++){
				var sel="";
				if(myHours==i){sel=" selected"}
                                    document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
                                    sel="";
				}
                            </script>
			</select>時
			<select name="minite_e">
                            <script type="text/javascript">
				var myD = new Date();
				var myMinutes = myD.getMinutes();
				for(var i=0;i<61;i+=15){
				var sel="";
				if(myMinutes<=i&&myMinutes>(i-15)){sel=" selected"}
                                    document.write("<option value=\""+i+"\""+sel+">"+i+"</option>");
                                    sel="";
                                    }
				</script>
			</select>分<br />

			<span id="button_otimein">
                            <button type="submit" class="img_but" id="nototime_submit">
                                <img src="<%= request.getContextPath() %>/images/add_19.gif" width="60" height="18">
                            </button>
				
                            <button type="submit" class="img_but" id="nototime_delete">
                                <img src="<%= request.getContextPath() %>/images/del_19.gif" width="60" height="18">
                            </button>
			
                            <button type="reset" class="img_but" id="nototime_reset">
                                <img src="<%= request.getContextPath() %>/images/reset_19.gif">
                            </button>
  			</span>
                    </form>
		</div>
				<!-- 残業情報カレンダー表示 -->

						<div class="category"><span class="caltitle">海外支援グループ　残業一覧</span><br />
						<script type="text/javascript">
							myDate    = new Date();                                    // 今日の日付データ取得
							myWeekTbl = new Array("日","月","火","水","木","金","土");  // 曜日テーブル定義
							myMonthTbl= new Array(31,28,31,30,31,30,31,31,30,31,30,31);// 月テーブル定義
							myYear = myDate.getFullYear();                                 // 年の取得
							if (((myYear%4)==0 && (myYear%100)!=0) || (myYear%400)==0){ // うるう年だったら...
							   myMonthTbl[1] = 29;                                     // 　２月を２９日とする
							}
							myMonth = myDate.getMonth();                               // 月を取得(0月～11月)
							myToday = myDate.getDate();                                // 今日の'日'を退避
							myDate.setDate(1);                                         // 日付を'１日'に変えて、
							myWeek = myDate.getDay();                                  // 　'１日'の曜日を取得
							myTblLine = Math.ceil((myWeek+myMonthTbl[myMonth])/7);     // カレンダーの行数
							myTable   = new Array(7*myTblLine);                        // 表のセル数分定義

							var year_now=myYear;
							var month_now=myMonth+1;
							var nototime=[1,9,17,23];                                          //残業不可日の配列
							var otime=[2,12,18,24];                                             //残業日の配列
							var omember=["山田太郎&nbsp;&nbsp;21：00まで<br />山田jiro&nbsp;&nbsp;21：00まで","山田花子&nbsp;&nbsp;20：00まで","山田勘三郎&nbsp;&nbsp;23：00まで"];                //残業する人間の配列


							document.write('<form action="" submit="post" class="calform">');
							document.write('<input type="submit" id="last_month" value="<<">');
							document.write('<span id="year_month">',year_now,'年',month_now,'月</span>');
							document.write('<input type="submit" id="next_month" value=">>">');
							document.write('</form>');

							for(i=0; i<7*myTblLine; i++) myTable[i]="　";              // myTableを掃除する
								for(i=0; i<myMonthTbl[myMonth]; i++)myTable[i+myWeek]=i+1; // 日付を埋め込む


									document.write("<table id='calendar'>");      // 表の作成開始
									document.write("<tr>");
									for(i=0; i<7; i++){                                        // 一行(１週間)ループ
   										document.write("<th align='center' ");
   											if(i==0)document.write("bgcolor='#fa8072'>");           // 日曜のセルの色
   											else    document.write("bgcolor='#ffebcd'>");           // 月～土のセルの色
   										document.write("<strong>",myWeekTbl[i],"</strong>");    // '日'から'土'の表示
   										document.write("</th>");
									}
									document.write("</tr>");
									var cnt_member=0;//残業する人を表示するためのカウンタ
								for(i=0; i<myTblLine; i++){                                // 表の「行」のループ
   									document.write("<tr>");                                 // 行の開始
   								for(j=0; j<7; j++){                                     // 表の「列」のループ
      								document.write("<td align='center' ");               // 列(セル)の作成
      								myDat = myTable[j+(i*7)];                            // 書きこむ内容の取得
      								if (otime.indexOf(myDat)>=0)document.write(" class='red'>"); // 残業あがある日
      								else if(j==0||j==6||nototime.indexOf(myDat)>=0)      document.write(" data-status='not' class='gray'>"); // 残業不可日
      								else               document.write("bgcolor='#ffffe0'>"); // 平日のセルの色

      								if (otime.indexOf(myDat)>=0){document.write("<strong><a href='#' class='example' data-tooltip='",omember[cnt_member],"'>",myDat,"</a></strong>");cnt_member++;        // 残業する日をを表示
      								}else {document.write("<strong>",myDat,"</strong>");}        // 日付セット
      								document.write("</td>");                             // 列(セル)の終わり
							}
							document.write("</tr>");                                // 行の終わり
						}
						document.write("</table>");                                // 表の終わり

				document.write("</div>");
				</script>
					<!-- 残業情報カレンダー表示終了 -->
				</li><!--登録ページ覧終了-->
			</ul><!--具体内容終了-->
		</div><!--タグの終了-->

		<!--footer-->
		<div id="footer">
                    <div class="boxC">
			<div class="boxR">
                            <img src="<%= request.getContextPath() %>/images/footer_copy.gif" alt="Copyright(c) Nipponham Business Experts Ltd. All Rights Reserved." class="fl mt3">
                            <a href="http://www.nipponham.co.jp/group/" target="_blank"><img src="<%= request.getContextPath() %>/images/footer_nh.gif" alt="Nippon Ham Group" class="fr"></a>
			</div>
                      </div>
		</div>
	</body>
</html>